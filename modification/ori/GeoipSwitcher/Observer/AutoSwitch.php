<?php

namespace MagestyApps\GeoipSwitcher\Observer;

use Magento\Framework\Event\ObserverInterface;
use Magento\Framework\Event\Observer;
use Magento\Store\Model\StoreManagerInterface;
use MagestyApps\GeoipSwitcher\Helper\Database as DbHelper;
use MagestyApps\GeoipSwitcher\Model\Blocker;
use MagestyApps\GeoipSwitcher\Model\CurrencySwitcher;
use MagestyApps\GeoipSwitcher\Model\Geoip;
use MagestyApps\GeoipSwitcher\Model\StoreSwitcher;

class AutoSwitch implements ObserverInterface
{
    /**
     * @var DbHelper
     */
    private $_dbHelper;

    /**
     * @var Geoip
     */
    private $_geoip;

    /**
     * @var StoreSwitcher
     */
    private $_storeSwitcher;

    /**
     * @var CurrencySwitcher
     */
    private $_currencySwitcher;

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @var Blocker
     */
    private $_blocker;

    /**
     * AutoSwitchStore constructor.
     * @param DbHelper $dbHelper
     * @param Geoip $geoipModel
     * @param StoreSwitcher $storeSwitcher
     * @param StoreManagerInterface $storeManager
     * @param Blocker $blocker
     */
    public function __construct(
        DbHelper $dbHelper,
        Geoip $geoipModel,
        StoreSwitcher $storeSwitcher,
        StoreManagerInterface $storeManager,
        CurrencySwitcher $currencySwitcher,
        Blocker $blocker
    ) {
        $this->_dbHelper = $dbHelper;
        $this->_geoip = $geoipModel;
        $this->_storeSwitcher = $storeSwitcher;
        $this->_storeManager = $storeManager;
        $this->_currencySwitcher = $currencySwitcher;
        $this->_blocker = $blocker;
    }

    /**
     * Automatically switch store based on visitor's location
     *
     * @param Observer $observer
     * @return $this
     */
    public function execute(Observer $observer)
    {
        $this->_processBlocker();
        $this->_processStoreSwitcher();
        $this->_processCurrencySwitcher();

        return $this;
    }

    /**
     * Process GeoIP blocker observer
     *
     * @return $this
     */
    private function _processBlocker()
    {
        $this->_blocker->blockRestrictedUsers();

        return $this;
    }

    /**
     * Process store switcher observer
     *
     * @return $this
     */
    private function _processStoreSwitcher()
    {
        if (!$this->_storeSwitcher->canSwitch()) {
            return $this;
        }

        $currentStoreCode = $this->_storeManager->getStore()->getCode();
        $customerStoreCode = $this->_storeSwitcher->getCustomerStoreCode();

        if (!$customerStoreCode) {
            return $this;
        }

        if ($currentStoreCode != $customerStoreCode) {
            $this->_storeSwitcher->switchStore($customerStoreCode);
        }

        return $this;
    }

    /**
     * Process currencys switcher observer
     *
     * @return $this
     */
    private function _processCurrencySwitcher()
    {
        if (!$this->_currencySwitcher->canSwitch()) {
            return $this;
        }

        $location = $this->_geoip->getCurrentLocation();
        $country = $location->getCountry();
        $currency = $this->_currencySwitcher->getCurrencyByCountry($country);

        $this->_currencySwitcher->switchCurrency($currency);

        return $this;
    }
}
