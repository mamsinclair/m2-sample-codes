<?php

/**
 * @author     Kristof Ringleff
 * @package    Fooman_PdfPickingList
 * @copyright  Copyright (c) 2016 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Fooman\PdfPickingList\Block\PdfCore;

use \Magento\Sales\Model\Order\ItemFactory;

class OrderPickingList extends PickingList
{

    const LAYOUT_HANDLE = 'fooman_pdforderpickinglist_list';

    /**
     * @param array $styling
     *
     * @return mixed
     */
    public function getItemsBlock($styling = [])
    {
        $block = $this->getLayout()->createBlock(
            '\Fooman\PdfPickingList\Block\OrderTable',
            'pdfpickinglist.items',
            ['data' => ['tableColumns' => $this->getTableColumns()]]
        );
        $block->setStyling($styling);
        $block->setCollection($this->getVisibleItems());
        return $block->toHtml();
    }

    /**
     * @return array
     */
    public function getTableColumns()
    {
        $return = [];
        $config = $this->helper->getOrderColumnConfig();
        if ($config) {
            $config = json_decode($config, true);
            foreach ($config as $column) {
                if (isset($column['width']) && $column['width'] > 0) {
                    $return[] = [
                        'index' => $column['columntype'],
                        'width' => $column['width'],
                        'title' => isset($column['title']) ? $column['title'] : ''
                    ];
                } else {
                    $return[] = [
                        'index' => $column['columntype'],
                        'title' => isset($column['title']) ? $column['title'] : ''
                    ];
                }
            }
        }
        return $return;
    }

    /**
     * get line items to display
     *
     * @return array
     */
    protected function getVisibleItems()
    {
        return $this->getOrderCollection()->getItems();
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->helper->getOrderTitle();
    }

    public function getForcedPageOrientation()
    {
        return $this->helper->getOrderPageOrientation();
    }

    public function getLogoBlock()
    {
        return false;
    }
}
