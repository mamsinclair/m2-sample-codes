<?php

/**
 * @author     Kristof Ringleff
 * @package    Fooman_PdfPickingList
 * @copyright  Copyright (c) 2016 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Fooman\PdfPickingList\Block\PdfCore;

use \Magento\Sales\Model\Order\ItemFactory;

class OrderItemList extends PickingList
{

    /**
     * @return array
     */
    public function getTableColumns()
    {
        $return = [];
        $config = $this->helper->getOrderItemColumnConfig();
        if ($config) {
            $config = json_decode($config, true);
            foreach ($config as $column) {
                if (isset($column['width']) && $column['width'] > 0) {
                    $return[] = [
                        'index' => $column['columntype'],
                        'width' => $column['width'],
                        'title' => isset($column['title']) ? $column['title'] : ''
                    ];
                } else {
                    $return[] = [
                        'index' => $column['columntype'],
                        'title' => isset($column['title']) ? $column['title'] : ''
                    ];
                }
            }
        }
        return $return;
    }
}
