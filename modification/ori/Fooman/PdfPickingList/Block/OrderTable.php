<?php

namespace Fooman\PdfPickingList\Block;

use Fooman\PdfPickingList\Block\PdfCore\OrderItemListFactory;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Fooman\PdfPickingList\Helper\Pdf as PdfConfig;

class OrderTable extends \Fooman\PdfCore\Block\Pdf\Table
{

    /**
     * @var OrderItemListFactory
     */
    private $pdfOrderItemListFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var PdfConfig
     */
    private $pdfConfig;

    public function __construct(
        Context $context,
        OrderItemListFactory $pdfOrderItemListFactory,
        CollectionFactory $orderCollectionFactory,
        PdfConfig $pdfConfig,
        array $data = []
    ) {
        $this->pdfOrderItemListFactory = $pdfOrderItemListFactory;
        $this->collectionFactory = $orderCollectionFactory;
        $this->pdfConfig = $pdfConfig;
        parent::__construct($context, $data);
    }

    public function getColumns()
    {
        if (is_null($this->columns)) {
            $this->columns = [];
            $i = 0;
            foreach ($this->tableColumns as $tableColumn) {
                if (isset($this->columnsMap[$tableColumn['index']])) {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __('Each column type can only appear once.')
                    );
                }
                $blockClass = sprintf(
                    '%s\%s',
                    'Fooman\PdfPickingList\Block\PdfCore\Column\Order',
                    \Magento\Framework\Api\SimpleDataObjectConverter::snakeCaseToUpperCamelCase($tableColumn['index'])
                );

                $index = $tableColumn['index'];

                $block = $this->getLayout()->createBlock($blockClass);

                $block->setId($tableColumn['index'])->setIndex($index);
                if (isset($tableColumn['width'])) {
                    $block->setWidthAbs($tableColumn['width']);
                }
                $this->setCustomRenderer($block);
                if ($this->getCurrencyCode()) {
                    $block->setCurrencyCode($this->getCurrencyCode());
                }

                if (isset($tableColumn['title']) && strlen($tableColumn['title']) >= 1) {
                    $block->setTitle($tableColumn['title']);
                }

                $this->columns[$i] = $block;
                $this->columnsMap[$tableColumn['index']] = $i;
                $i++;
            }
        }
        return $this->columns;
    }

    public function getAlign($isFirst, $isLast)
    {
        return 'left';
    }

    public function hasExtras(\Magento\Framework\DataObject $item)
    {
        return $this->pdfConfig->getOrderItemDetails();
    }

    public function getExtras(\Magento\Framework\DataObject $item)
    {
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('entity_id', $item->getEntityId());
        $pickingList = $this->pdfOrderItemListFactory->create(
            ['data' => ['order_collection' => $collection]]
        );

        return $pickingList->getItemsBlock();

    }
    
}
