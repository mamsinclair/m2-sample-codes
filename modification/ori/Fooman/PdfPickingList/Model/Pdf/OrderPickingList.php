<?php

/**
 * @author     Kristof Ringleff
 * @package    Fooman_PdfPickingList
 * @copyright  Copyright (c) 2016 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Fooman\PdfPickingList\Model\Pdf;

use Fooman\PdfCore\Model\PdfRenderer;
use Fooman\PdfPickingList\Block\PdfCore\OrderPickingListFactory;

class OrderPickingList
{
    /**
     * @param PdfRenderer             $pdfRenderer
     * @param OrderPickingListFactory $orderPickingListFactory
     */
    public function __construct(
        PdfRenderer $pdfRenderer,
        OrderPickingListFactory $orderPickingListFactory
    ) {
        $this->pdfRenderer = $pdfRenderer;
        $this->orderPickingListFactory = $orderPickingListFactory;
    }

    /**
     * @param  \Magento\Sales\Model\ResourceModel\Order\Collection $collection
     * @return string
     */
    public function generate(\Magento\Sales\Model\ResourceModel\Order\Collection $collection)
    {
        $document = $this->orderPickingListFactory->create(
            ['data' => ['order_collection' => $collection]]
        );

        $this->pdfRenderer->addDocument($document);
        return $this->pdfRenderer->getPdfAsString();
    }
}
