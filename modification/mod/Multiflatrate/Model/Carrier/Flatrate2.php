<?php
namespace Magecomp\Multiflatrate\Model\Carrier;
use Magento\Quote\Model\Quote\Address\RateRequest;
use Magento\Shipping\Model\Rate\Result;
class Flatrate2 extends \Magento\Shipping\Model\Carrier\AbstractCarrier implements \Magento\Shipping\Model\Carrier\CarrierInterface
{
     protected $_code = 'flatrate2';
     protected $_isFixed = true;
     protected $_rateResultFactory;
     protected $_rateMethodFactory;
     protected $_session;
     protected $_sessionQuote;

     public function __construct(\Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
     \Magento\Quote\Model\Quote\Address\RateResult\ErrorFactory $rateErrorFactory,
     \Psr\Log\LoggerInterface $logger,
     \Magento\Shipping\Model\Rate\ResultFactory $rateResultFactory,
     \Magento\Quote\Model\Quote\Address\RateResult\MethodFactory $rateMethodFactory,
     \Magento\Checkout\Model\Session $session,
     \Magento\Backend\Model\Session\Quote $sessionQuote,
     array $data = [])
     {
        $this->_rateResultFactory = $rateResultFactory;
        $this->_rateMethodFactory = $rateMethodFactory;
        $this->_session = $session;
        $this->_sessionQuote = $sessionQuote;
        parent::__construct($scopeConfig, $rateErrorFactory, $logger, $data);
    }

    public function collectRates(\Magento\Quote\Model\Quote\Address\RateRequest $request)
    {

        if (!$this->getConfigFlag('active')){
            return false;
        }

        $freeBoxes = 0;
        if ($request->getAllItems()) {
            foreach ($request->getAllItems() as $item) {
                if ($item->getProduct()->isVirtual() || $item->getParentItem()) {
                    continue;
                }

                if ($item->getHasChildren() && $item->isShipSeparately()) {
                    foreach ($item->getChildren() as $child) {
                        if ($child->getFreeShipping() && !$child->getProduct()->isVirtual()) {
                            $freeBoxes += $item->getQty() * $child->getQty();
                        }
                    }
                } elseif ($item->getFreeShipping()) {
                    $freeBoxes += $item->getQty();
                }
            }
        }
        $this->setFreeBoxes($freeBoxes);
        /** @var Result $result */
        $result = $this->_rateResultFactory->create();
        if ($this->getConfigData('type') == 'O') {
            // per order
            $shippingPrice = $this->getConfigData('price');
        } elseif ($this->getConfigData('type') == 'I') {
            // per item
            $shippingPrice = $request->getPackageQty() * $this->getConfigData(
                'price'
            ) - $this->getFreeBoxes() * $this->getConfigData(
                'price'
            );
        } else {
            $shippingPrice = false;
        }
        $onlyshipprice = $shippingPrice; // Fixed - Per Item
        $total_front = $this->_session->getQuote()->getData();
        $total_back = $this->_sessionQuote->getQuote()->getData();
        $total = [];
        if(array_key_exists('subtotal',$total_front))
        {
            $total = $total_front;
        }
        else
        {
            $total = $total_back;
        }
        $shippingPrice = $this->getFinalPriceWithHandlingFee($shippingPrice);

        if($this->getConfigData('handling_type') == 'FI') // Fixed - Per Item
        {
            if($request->getPackageQty() > 1)
            {
                $onlyhandelfee = $shippingPrice - $onlyshipprice;
                $feeadded = $onlyhandelfee * ($request->getPackageQty() - 1);
                $shippingPrice = $shippingPrice + $feeadded;
            }
        }

        $minamount = $this->getConfigData('min_amount');
        $maxamount = $this->getConfigData('max_amount');
        if($minamount == '')
        {
            $minamount = 0;
        }
        if($maxamount == '')
        {
            $maxamount = 5000;
        }

        if ($shippingPrice !== false && $total['subtotal'] >= $minamount && $total['subtotal'] <= $maxamount)
        {
            $method = $this->_rateMethodFactory->create();
            $method->setCarrier('flatrate2');
            $method->setCarrierTitle($this->getConfigData('title'));
            $method->setMethod('flatrate2');
            $method->setMethodTitle($this->getConfigData('name'));
            $method->setPrice($shippingPrice);
            $method->setCost($shippingPrice);
            $result->append($method);
        }
        return $result;
    }

    public function getAllowedMethods()
    {
        return ['flatrate2' => $this->getConfigData('name')];
    }
}
