<?php

namespace MagestyApps\GeoipSwitcher\Helper;


class Shipping extends \Magento\Directory\Helper\Data{
    
    public function getDefaultCountry($store = null)
    {
        $session = \Magento\Framework\App\ObjectManager::getInstance()
                ->get('\Magento\Customer\Model\Session');
        
        $location = $session->getLocationData();
        if($location && $location->getCountry()){
            return $location->getCountry();
        }
        return parent::getDefaultCountry();
        //return 'UA';
        
    }
    
}
