<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace MagestyApps\GeoipSwitcher\Plugin\Magento\Tax\Model;

class TaxConfigProvider
    extends \Magento\Tax\Model\TaxConfigProvider
{
    /**
     * @var TaxHelper
     */
    protected $taxHelper;

    /**
     * @var Config
     */
    protected $taxConfig;

    /**
     * @var ScopeConfigInterface
     */
    protected $scopeConfig;

    /**
     * @var CheckoutSession
     */
    protected $objectManaged;

    /**
     * @param TaxHelper $taxHelper
     * @param Config $taxConfig
     * @param CheckoutSession $checkoutSession
     * @param ScopeConfigInterface $scopeConfig
     */
    public function __construct(
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\ObjectManagerInterface $objectManaged
    ) {
        $this->scopeConfig = $scopeConfig;
        $this->objectManager = $objectManaged;
    }

    /**
     * {@inheritdoc}
     */
    public function afterGetConfig(
        \Magento\Tax\Model\TaxConfigProvider $subject,
        $result
    )
    {
        $location = $this->objectManager
            ->get('\Magento\Customer\Model\Session')
                ->getLocationData();
        if ($location && $location->getCountry()) {
            $result['defaultCountryId'] =  $location->getCountry();
        } else {
            $result['defaultCountryId'] =  $this->scopeConfig->getValue(
                \Magento\Tax\Model\Config::CONFIG_XML_PATH_DEFAULT_COUNTRY,
                \Magento\Store\Model\ScopeInterface::SCOPE_STORE
            );
        }

        \Magento\Framework\App\ObjectManager::getInstance()
            ->create('Psr\Log\LoggerInterface')
            ->debug('
        *****************************************************
        *** TAX CONFIG PROVIDER - after getConfig() plugin
        *****************************************************
        defaultCountryId set to '.var_export($result['defaultCountryId'], true), ['method' =>__METHOD__]);
        return $result;
    }
}
