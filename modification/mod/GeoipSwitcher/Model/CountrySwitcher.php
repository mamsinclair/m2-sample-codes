<?php
/**
 * Copyright © 2016 MagestyApps. All rights reserved.
 *  * See COPYING.txt for license details.
 */

namespace MagestyApps\GeoipSwitcher\Model;

use Magento\Framework\Stdlib\CookieManagerInterface;
use Magento\Framework\Stdlib\Cookie\CookieMetadataFactory;
use Magento\Store\Model\StoreManagerInterface;
use \MagestyApps\GeoipSwitcher\Helper\CurrencySwitcher as Helper;
use MagestyApps\GeoipSwitcher\Helper\Database;

class CountrySwitcher
{
    const CURRENCY_COOKIE_CODE = 'magestyapps_currency_code';

    /**
     * @var Helper
     */
    private $_helper;

    /**
     * @var Database
     */
    private $_geoipHelper;

    /**
     * @var StoreManagerInterface
     */
    private $_storeManager;

    /**
     * @var CookieManagerInterface
     */
    private $_cookieManager;

    /**
     * @var CookieMetadataFactory
     */
    private $_cookieMetadataFactory;

    public function __construct(
        Helper $helper,
        StoreManagerInterface $storeManager,
        CookieManagerInterface $cookieManager,
        CookieMetadataFactory $cookieMetadataFactory,
        Database $geoipHelper
    ) {
        $this->_helper = $helper;
        $this->_storeManager = $storeManager;
        $this->_cookieManager = $cookieManager;
        $this->_cookieMetadataFactory = $cookieMetadataFactory;
        $this->_geoipHelper = $geoipHelper;
    }

    /**
     * Check whether the extension should switch currency automatically
     *
     * @return bool
     */
    public function canSwitch()
    {
        if (!$this->_helper->isEnabled()) {
            return false;
        }

        if ($this->_helper->isCrawler()) {
            return false;
        }

        if (!$this->_helper->checkExceptionUrl()) {
            return false;
        }

        return true;
    }

    /**
     * Switches currency
     *
     * @param $geoipCurrency
     * @return $this
     */
    public function switchCurrency($geoipCurrency)
    {
        $currencyCookie = $this->_cookieManager->getCookie(self::CURRENCY_COOKIE_CODE);
        $currency = ($currencyCookie) ? $currencyCookie : $geoipCurrency;

        $store = $this->_storeManager->getStore();
        if (!$currency || $store->getCurrentCurrencyCode() == $currency) {
            return $this;
        }

        $store->setCurrentCurrencyCode($currency);
        $this->setCurrencyCookie($currency);

        return $this;
    }

    /**
     * Set extension's currency cookie
     *
     * @param $currency
     * @return $this
     */
    public function setCurrencyCookie($currency)
    {
        $metadata = $this->_cookieMetadataFactory->createPublicCookieMetadata()->setPath('/');
        $this->_cookieManager->setPublicCookie(self::CURRENCY_COOKIE_CODE, $currency, $metadata);
        return $this;
    }

    /**
     * Get currency by country code
     *
     * @param $countryCode
     * @return string
     */
    public function getCurrencyByCountry($countryCode)
    {
        $curBase = $this->_helper->getRelationsDatabase();

        if ($curBase === false || !count($curBase)) {
            return false;
        }

        $codes = $this->_storeManager->getStore()->getAvailableCurrencyCodes(true);
        foreach ($curBase as $value) {
            $data = explode(';', $value);
            $curVal = trim($data[1]);

            $currentCountry = $this->_geoipHelper->prepareCountryCode($countryCode);
            $compareCountry = $this->_geoipHelper->prepareCountryCode($data[0]);

            if ($currentCountry != $compareCountry) {
                continue;
            }

            if (strstr($curVal, ',')) {
                $curCodes = explode(',', $curVal);
                if (!$curCodes) {
                    continue;
                }

                foreach ($curCodes as $code) {
                    $code = trim($code);
                    if (in_array($code, $codes)) {
                        return $code;
                    }
                }
            } else {
                if (in_array($curVal, $codes)) {
                    return $curVal;
                }
            }
        }
    }
}
