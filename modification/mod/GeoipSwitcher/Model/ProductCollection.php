<?php

namespace MagestyApps\GeoipSwitcher\Model;

class ProductCollection{

    const VENDOR_PRODUCT_INDEX_TABLE = 'amasty_amshopby_update_product_vendor_index';
    const VENDOR_SHIPPINGLIST_TABLE  = 'marketplace_shippinglist';
    const VENDOR_SHIPPING_METHODS_TABLE = 'marketplace_shippinglist_method';
    const VENDOR_TABLE  = 'marketplace_userdata';
    const CURRENCY_TABLE  = 'directory_currency_rate';


    protected $_resource;
    protected $_session;

    public function __construct(
      \Magento\Framework\App\ResourceConnection $resource,
      \Magento\Customer\Model\Session $session
    ) {

        $this->_session  = $session;
        $this->_resource = $resource;
    }

    public function updateAssignedCollection($productCollection, $group = true, $need_filter = false){
        $countryCode = $this->getCountryCode($need_filter);
        $vendorInSql = $this->getVendorIdInSql($countryCode);

        $productCollection->getSelect()
                ->joinLeft(array('table_vendor_index' => $productCollection->getTable(self::VENDOR_PRODUCT_INDEX_TABLE)), 'table_vendor_index._product_id=main_table.product_id AND table_vendor_index.vendor_id=main_table.seller_id', array('table_vendor_index.vendor_id', 'table_vendor_index._min_price'));
        if ($group) {
            $productCollection->getSelect()->group('entity_id');
        }
        if($countryCode){
            $productCollection->getSelect()->where('table_vendor_index.vendor_id IN (' . $vendorInSql . ')');
        }
        return $productCollection;
    }

    public function updateCollection($productCollection, $need_filter = false, $countryCode = false){
        //var_dump($countryCode); die();
        if(!$countryCode){
            $countryCode = $this->getCountryCode($need_filter);
        }
        $vendorInSql = $this->getVendorIdInSql($countryCode);

        if (!$productCollection || !$productCollection->getTableVendorsAdded()){
            $productCollection->getSelect()
                ->joinLeft(array('table_vendor_index' => $productCollection->getTable(self::VENDOR_PRODUCT_INDEX_TABLE)), '_product_id=e.entity_id', array('table_vendor_index.vendor_id', 'table_vendor_index._min_price'));
            $productCollection->groupByAttribute('entity_id');
            $productCollection->setTableVendorsAdded();

        }
                //->group('_product_id');
                ;
        if($countryCode){
            $productCollection->getSelect()->where('table_vendor_index.vendor_id IN (' . $vendorInSql . ')');
        }
        return $productCollection;
    }

    public function getSqlIn($need_filter=false, $countryCode = false){
       if(!$countryCode){
            $countryCode = $this->getCountryCode($need_filter);
        }
        if($countryCode){
            return $this->getVendorIdInSql($countryCode);
        } else {
            return "";
        }
    }

    protected function getVendorIdInSql($countryCode){
        $conn = $this->_resource
                ->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);

        $select = $conn->select()

            ->from(
                ['vendor_shipping' => self::VENDOR_SHIPPINGLIST_TABLE],
                'partner_id'
            )
            ->where('vendor_shipping.dest_country_id=?', $countryCode);

        return $select->__toString();
    }

    public function getVendorShippingList($vendorId){
        $countryCode = $this->getCountryCode(true);

        $conn = $this->_resource
                ->getConnection(\Magento\Framework\App\ResourceConnection::DEFAULT_CONNECTION);
        // @TODO convert price to the base currency
/*
        $select = $conn
            ->select()
                ->from(
                ['vendor_shipping' => self::VENDOR_SHIPPINGLIST_TABLE],
                ['partner_id', 'price']
            )->where('vendor_shipping.dest_country_id=?', $countryCode)
             ->where('vendor_shipping.partner_id=?', $vendorId);
        $select->joinLeft(
            ['vendor_shipping_names' => self::VENDOR_SHIPPING_METHODS_TABLE],
            'vendor_shipping.shipping_method_id = vendor_shipping_names.entity_id',
            ['vendor_shipping_names.method_name']
        )->order("price ASC");
*/
        $select = $conn
            ->select()
                ->from(
                ['vendor_shipping' => self::VENDOR_SHIPPINGLIST_TABLE],
                    ['partner_id', 'price' => 'IF(_mu.currency IS NOT NULL, price/_dcr.rate, price)']
            )->where('vendor_shipping.dest_country_id=?', $countryCode)
             ->where('vendor_shipping.partner_id=?', $vendorId);
        $select
            ->joinLeft(
            ['vendor_shipping_names' => self::VENDOR_SHIPPING_METHODS_TABLE],
            'vendor_shipping.shipping_method_id = vendor_shipping_names.entity_id',
            ['vendor_shipping_names.method_name']
            )->joinLeft(
                ['_mu' => self::VENDOR_TABLE],
                '_mu.seller_id = '.$vendorId,
                []
            )->joinLeft(
                ['_dcr' => self::CURRENCY_TABLE],
                '_mu.currency IS NOT NULL AND _mu.currency = _dcr.currency_to',
                []    //_dcr.rate
        )->order("price ASC");

        return $conn->fetchAll($select);
    }

    public function getCountryCode($get_from_session = false){
        if(!$get_from_session){
            return "";
        }
        $location = $this->_session->getLocation();

        $location = \Magento\Framework\App\ObjectManager::getInstance()
            ->create('Magento\Customer\Model\Session')
            ->getLocation();

        if($location){
            $return_country = $location->getCountry();
        } else{
            $return_country = "";
        }

        return $return_country;
    }
}
