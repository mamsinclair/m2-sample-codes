<?php

namespace MagestyApps\GeoipSwitcher\Model;

use Magento\Customer\Model\Session;
use Magento\Framework\DataObject;
use MagestyApps\GeoipSwitcher\Helper\Database as DbHelper;
use Magento\Directory\Model\Region;

class Geoip
{
    const SESSION_PARAM_CODE = 'customer_geoip_location';

    /**
     * @var Session
     */
    private $_session;

    /**
     * @var DbHelper
     */
    private $_dbHelper;

    /**
     * @var Region
     */
    private $_regionModel;

    /**
     * Geoip constructor.
     * @param Session $session
     * @param DbHelper $dbHelper
     * @param Region $regionModel
     */
    public function __construct(Session $session, DbHelper $dbHelper, Region $regionModel)
    {
        $this->_session = $session;
        $this->_dbHelper = $dbHelper;
        $this->_regionModel = $regionModel;
    }

    /**
     * Get current visitor's location
     *
     */
    public function getCurrentLocation()
    {
        if (!$this->_session->getData(self::SESSION_PARAM_CODE)) {
            $location = $this->getLocation();
            $this->_session->setData(self::SESSION_PARAM_CODE, $location);
        }
        //    $location = $this->getLocation();
        //    $this->_session->setData(self::SESSION_PARAM_CODE, $location);
        
        return new DataObject($this->_session->getData(self::SESSION_PARAM_CODE));
    }
    
    public function setLocation($countryCode, $countryName = null){
        $location['country'] = $countryCode;
		if ($countryName) {
			$location['country_name'] = $countryName;
		} else {
			$location['country_name'] = ' ';
		}
		
        $this->_session->setData(self::SESSION_PARAM_CODE, $location); 
        return $this;
    }

    /**
     * Get a location by IP address
     *
     * @param null $ipAddress
     * @return array
     */
    public function getLocation($ipAddress = null)
    {
        $reader = new MaxMind\Db\Reader($this->_dbHelper->getDatabasePath());

        if (!$ipAddress) {
            $ipAddress = $this->_dbHelper->getCustomerIp();
        }

        //$ipAddress = '93.79.112.134'; //hard coded for now
        //$ipAddress = '213.94.128.22';
        
        
        $result = $reader->get($ipAddress);
        $location = [];
        if (isset($result['country']) && isset($result['country']['iso_code'])) {
            $location['country'] = $result['country']['iso_code'];
        }
        
        if (isset($result['country']) 
                && isset($result['country']['names'])
                && isset($result['country']['names']['en'])) {
        
            $location['country_name'] = $result['country']['names']['en'];
        }

        if (isset($result['subdivisions'])
            && count($result['subdivisions'])
            && isset($location['country'])
        ) {
            $subdivision = reset($result['subdivisions']);
            $regionCode = $subdivision['iso_code'];
            $region = $this->_regionModel->loadByCode($regionCode, $location['country']);

            if ($region && $region->getRegionId()) {
                $location['region_id'] = $region->getRegionId();
            } elseif (isset($subdivision['names']) && isset($subdivision['names']['en'])) {
                $location['region'] = $subdivision['names']['en'];
            }
        }

        if (isset($result['city'])
            && isset($result['city']['names'])
            && isset($result['city']['names']['en'])
        ) {
            $location['city'] = $result['city']['names']['en'];
        }

        if (isset($result['postal']) && isset($result['postal']['code'])) {
            $location['postcode'] = $result['postal']['code'];
        }

        return $location;
    }
}
