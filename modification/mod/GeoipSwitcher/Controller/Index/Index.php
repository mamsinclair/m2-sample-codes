<?php


namespace MagestyApps\GeoipSwitcher\Controller\Index;

class Index extends \Magento\Framework\App\Action\Action {

    protected $_session;
    protected $_geoip;
    protected $jsonResultFactory;
    protected $_countryCollectionFactory;
    protected $_messageManager;
    protected $_scopeConfig;
    protected $_countrySwitcher;

    public function __construct(
        \Magento\Framework\App\Action\Context $context,
         \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,
        \Magento\Customer\Model\Session $session,
        \MagestyApps\GeoipSwitcher\Model\Geoip $geoip,
        //\Magento\Framework\Message\ManagerInterface $messageManager,
        \Magento\Framework\Controller\Result\JsonFactory $jsonResultFactory,
        \MagestyApps\GeoipSwitcher\Block\Top\CountrySwitcher $countrySwitcher
    )
    {
        $this->_session = $session;
        $this->_geoip = $geoip;
        $this->jsonResultFactory = $jsonResultFactory;
        $this->_countryCollectionFactory = $countryCollectionFactory;
        $this->_messageManager = $context->getMessageManager();//$messageManager;
        $this->_scopeConfig = $scopeConfig;
        $this->_countrySwitcher = $countrySwitcher;
        parent::__construct($context);
    }

    public function execute(){
        $error = false;
        $targetLocation = $this->getRequest()->getParam('targetLocation');
        switch ($targetLocation) {
            case 'ip':
                $data = $this->getLocationById();
                break;
            case 'session':
                $location = $this->_geoip->getCurrentLocation();
                //\Zend_Debug::dump($location, 'location');
                //\Zend_Debug::dump($location->getCountry(), '$location->getCountry()');

                if ($location->getCountry()) {
                    if (!$location->getCountryName()) {
                        $location->setCountryName($this->getCountryNameByCode($location->getCountry()));
                    }
                    $location->setCountryFlag($this->_countrySwitcher->getCountryFlag($location->getCountry()));
                    $data = [
                        'success' => true,
                        'country_info' => $location->toArray(),
                    ];
                } else {
                    // no session data, get it directly from IP
                    $data = $this->getLocationById();
                }
                break;
            case null:
            default:
                $country = $this->getRequest()->getParam('country-code');
                if(empty($country)){
                    $error = __('No country code sent');
                }
                $countryName = $this->getCountryNameByCode($country);
                if(!$countryName){
                    $error = __('Unknown country code: ' . $country);
                }
                $this->_geoip->setLocation($country, $countryName);
                $location = $this->_geoip->getCurrentLocation();
                $this->_session->setLocationData($location);
                if($error){
                    $this->_messageManager->addError($error);
                    $data = [
                        'error' => $error
                    ];
                }else{
                    $data = [
                        'success' => true
                    ];
                }
                break;
        }

        $result = $this->jsonResultFactory->create();
        $result->setData($data);
        return $result;
    }

    public function getLocationById() {
        $locationByIp = $this->_geoip->getLocation();

        if (is_array($locationByIp) &&
            isset($locationByIp['country']) &&
            isset($locationByIp['country_name'])) {
            $locationByIp['country_flag'] = $this->_countrySwitcher->getCountryFlag($locationByIp['country']);
            $data = [
                'success' => true,
                'country_info' => $locationByIp,
            ];
            /////////////////////////////////////////////////////////////////////////////
            // Comment the following lines if we want to set the location into session //
            /////////////////////////////////////////////////////////////////////////////
            $this->_geoip->setLocation($locationByIp['country'], $locationByIp['country_name']);
            $location = $this->_geoip->getCurrentLocation();
            $this->_session->setLocationData($location);
            //////////////////////////////////////////////////////////////////////////////
            //////////////////////////////////////////////////////////////////////////////
        } else {
            $data = [
                'success' => false,
                'error' => __('Unable to resolve location by IP'),
            ];
        }

        return $data;
    }

    protected function getCountryNameByCode($code){
        $options = $this->getCountries();
        foreach($options as $option){
            if($option['value'] == $code){
                return $option['label'];
            }
        }
    }

    protected function getCountries(){
        return $options = $this->getCountryCollection()
                    ->setForegroundCountries($this->getTopDestinations())
                        ->toOptionArray();
    }

    protected function getCountryCollection(){
        $collection = $this->_countryCollectionFactory->create()->loadByStore();
        return $collection;
    }

    protected function getTopDestinations(){
        $destinations = (string)$this->_scopeConfig->getValue(
            'general/country/destinations',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return !empty($destinations) ? explode(',', $destinations) : [];
    }
}

