<?php

namespace MagestyApps\GeoipSwitcher\Block\Top;

class CountrySwitcher extends \Magento\Framework\View\Element\Template{
    
    protected $_session;
    protected $_countryCollectionFactory;
    protected $_directoryList;
    
    public function __construct(
        \Magento\Framework\View\Element\Template\Context $context,
        \Magento\Directory\Model\ResourceModel\Country\CollectionFactory $countryCollectionFactory,    
        \Magento\Customer\Model\Session $session, 
        \Magento\Framework\App\Filesystem\DirectoryList $directoryList,    
        array $data = array()
    ) {
        parent::__construct($context, $data);
        $this->_session = $session;
        $this->_countryCollectionFactory = $countryCollectionFactory;
        $this->_directoryList = $directoryList;
    //    var_dump($this->_session->getLocationData());
    }
    
    public function getLocationCountryName() {
        $location = $this->getLocation();
        if($location){
            return $location->getCountryName();
        }
    }
    
    public function getLocationCountryCode() {
        $location = $this->getLocation();
        if($location){
            return $location->getCountry();
        }
    }
    
    public function getCountries(){
        return $options = $this->getCountryCollection()
                    ->setForegroundCountries($this->getTopDestinations())
                        ->toOptionArray();
    }
    
    public function getCountryFlag($isoCode = ''){
        if($isoCode == '') {
            $location = $this->getLocation();
            if($location){
                $isoCode = $location->getCountry();
                return $this->getViewFileUrl('MagestyApps_GeoipSwitcher::flags/' . strtolower($isoCode) . '.png');
            }
        }else{
            return $this->getViewFileUrl('MagestyApps_GeoipSwitcher::flags/' . strtolower($isoCode) . '.png');
        }
    }
    
    public function getLocation(){

	$location = \Magento\Framework\App\ObjectManager::getInstance()
            ->create('Magento\Customer\Model\Session')
            ->getLocation();
        //return $this->_session->getLocationData();
        return $location;
    }
    
    protected function getCountryCollection(){
        $collection = $this->_countryCollectionFactory->create()->loadByStore();
        return $collection;
    }
    
    public function isFlagExists($isoCode) {
        $appPath = $this->_directoryList->getPath('app'); 
        $fullPath = $appPath . '/code/MagestyApps/GeoipSwitcher/view/frontend/web/flags/' . strtolower($isoCode) . '.png';
        
        if(file_exists($appPath . '/code/MagestyApps/GeoipSwitcher/view/frontend/web/flags/' . strtolower($isoCode) . '.png')) {
            return true;
        }
    }
    
    protected function getTopDestinations(){
        $destinations = (string)$this->_scopeConfig->getValue(
            'general/country/destinations',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE
        );
        return !empty($destinations) ? explode(',', $destinations) : [];
    }
}