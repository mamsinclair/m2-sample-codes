(function (factory) {
    if (typeof define === 'function' && define.amd) {
        define(['jquery'], factory);
    } else {
        factory(jQuery);
    }
}(function ($) {
    $.fn.customDropdown = function () {
        return this.each(function () {
            var el = $(this);
            
            var options = el.find('option');
            var container = $('<div class="custom-combobox-container"></div>');
            var containerSelected = $('<div class="custom-combobox-container-selected"></div>');
            var containerInner = $('<div class="custom-combobox-container-inner"></div>');
            for(var a=0; options.length>a; a++){
                var img = '';
                if($(options[a]).attr('data-imagesrc')){
                    img = '<img src="' + $(options[a]).attr('data-imagesrc') + '">';
                }
                $(options[a]).attr('data-select', 'data-' + a);
                var optionDiv = $('<div data-index="' + a + '" class="custom-combobox-option">' + img + '&nbsp;' + $(options[a]).text() + '</div>');
                containerInner.append(optionDiv);
                
                if($(options[a]).is( ":selected" )){
                    containerSelected.append(optionDiv.clone());
                }
            }
            container.append(containerSelected);
            container.append(containerInner);
            el.parent().append(container);
            
            function registerEvents() {
                containerSelected.on('click', function(){
                    var innerContainer = $(this).parent().find('.custom-combobox-container-inner');
                    if(innerContainer.is(':visible')){
                        innerContainer.hide();
                    }else{
                        innerContainer.show();
                    }
                });
                container.find('.custom-combobox-option').each(function(){
                    var el = $(this);
                    el.on('click', function(){
                        if($(this).parent().hasClass('custom-combobox-container-selected')){
                            return;
                        }
                        var dataIndex = $(this).attr('data-index');
                        var select = container.prev();
                        select.find('[data-select="data-' + dataIndex + '"]').prop('selected', true);
                        container.find('.custom-combobox-container-selected').html($(this).html());
                        $(this).parent().hide();
                    });
                })
            }
            
            registerEvents();
        });
    };
})
);