<?php

namespace Fooman\PdfPickingList\Block;

class Table extends \Fooman\PdfCore\Block\Pdf\Table
{
    protected $_template = 'Fooman_PdfPickingList::pdf/table.phtml';

    public function hasExtras(\Magento\Framework\DataObject $item)
    {
        $item = $this->getOrderItem($item);
        $options = $item->getProductOptions();
        return (!empty($options));
    }

    public function getExtras(\Magento\Framework\DataObject $item)
    {
        $html = '';
        $item = $this->getOrderItem($item);
        $options = $item->getProductOptions();
        $arrayKeys = ['options', 'additional_options', 'attributes_info'];
        $optOutput = [];
        foreach ($arrayKeys as $key) {
            if (isset($options[$key])) {
                foreach ($options[$key] as $option) {
                    $optOutput[] = $this->escapeHtml($option['label']) . ': '
                        . $this->escapeHtml($option['value']);
                }
            }
        }
        if (!empty($optOutput)) {
            $html .= '&nbsp;&nbsp;&nbsp;&nbsp;' . implode('<br/>&nbsp;&nbsp;&nbsp;&nbsp;', $optOutput);
        }
        return $html;
    }

    protected function getOrderItem($item)
    {
        if ($item instanceof \Magento\Sales\Api\Data\OrderItemInterface) {
            return $item;
        }
        return $item->getOrderItem();
    }

}
