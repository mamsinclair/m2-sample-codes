<?php

/**
 * @author     Kristof Ringleff
 * @package    Fooman_PdfPickingList
 * @copyright  Copyright (c) 2016 Fooman Limited (http://www.fooman.co.nz)
 *
 * For the full copyright and license information, please view the LICENSE
 * file that was distributed with this source code.
 */

namespace Fooman\PdfPickingList\Block\PdfCore;

use \Magento\Sales\Model\Order\ItemFactory;
use \Fooman\PdfPickingList\Helper\Pdf as PdfHelper;

class PickingList extends \Fooman\PdfCore\Block\Pdf\DocumentRenderer
{

    const LAYOUT_HANDLE = 'fooman_pdfpickinglist_list';

    /**
     * @var array
     */
    protected $items = [];

    /**
     * @var \Magento\Sales\Model\Order\ItemFactory
     */
    protected $itemFactory;

    /**
     * @var \Fooman\PdfPickingList\Helper\Pdf
     */
    protected $helper;

    /**
     * @var \Magento\Catalog\Model\ProductFactory
     */
    private $productFactory;

    /**
     * @var \Magento\Eav\Model\Entity\AttributeFactory
     */
    private $attributeFactory;

    /**
     * @param \Magento\Backend\Block\Template\Context       $context
     * @param \Magento\Framework\Filter\Input\MaliciousCode $maliciousCode
     * @param \Fooman\PdfCore\Model\Template                $template
     * @param array                                         $data
     */
    public function __construct(
        \Magento\Backend\Block\Template\Context $context,
        \Magento\Framework\Filter\Input\MaliciousCode $maliciousCode,
        \Fooman\PdfCore\Model\Template $template,
        \Magento\Framework\App\AreaList $areaList,
        ItemFactory $itemFactory,
        PdfHelper $helper,
        \Magento\Catalog\Model\ProductFactory $productFactory,
        \Magento\Eav\Model\Entity\AttributeFactory $attributeFactory,
        array $data = []
    ) {
        $this->itemFactory = $itemFactory;
        $this->helper = $helper;
        $this->productFactory = $productFactory;
        $this->attributeFactory = $attributeFactory;
        parent::__construct($context, $maliciousCode, $template, $areaList, $data);
    }

    /**
     * @return \Magento\Sales\Api\Data\OrderInterface[]
     */
    public function getOrderCollection()
    {
        return $this->getData('order_collection');
    }

    /**
     * get line items to display
     *
     * @return array
     */
    protected function getVisibleItems()
    {
        $allOrders = $this->getOrderCollection();

        foreach ($allOrders as $order) {
            foreach ($order->getAllVisibleItems() as $item) {
                $type = $item->getProduct()
                    ? $item->getProduct()->getTypeId()
                    : '';
                if ($type == \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE) {
                    foreach ($item->getChildrenItems() as $child) {
                        /**/$child->setName($item->getName());
                        /*$child->setProductPickPoint($item->getProduct()->getPickPoint());/**/
                        $child->setPrice($item->getPrice());
                        $this->addItem($child);
                    }
                } elseif ($type == \Magento\Bundle\Model\Product\Type::TYPE_CODE) {
                    $this->items[] = $this->itemFactory->create()->setName($item->getName());
                    foreach ($item->getChildrenItems() as $child) {
                        $child->setPrice($item->getPrice());
                        $this->addItem($child, true);
                    }
                } else {
                    $this->addItem($item);
                }
            }
        }

        return $this->items;
    }

    public function prepareItem($item)
    {
        $this->addProductAttributeValues($item);
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderItemInterface $item
     */
    protected function addProductAttributeValues(\Magento\Sales\Api\Data\OrderItemInterface $item)
    {

        $productAttributes = $this->getProductAttributes();
        if (count($productAttributes) == 0) {
            return;
        }

        if ($item->getProductType()
            === \Magento\ConfigurableProduct\Model\Product\Type\Configurable::TYPE_CODE
        ) {
            //we want to load the attribute of the simple product if part of a configurable
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->productFactory->create()->loadByAttribute(
                'sku',
                $item->getProductOptionByCode('simple_sku')
            );
        } else {
            /** @var \Magento\Catalog\Model\Product $product */
            $product = $this->productFactory->create()->load($item->getProductId());
        }



        if (!$product) {
            return;
        }

        //get parent if has
        $parentProduct = \Magento\Framework\App\ObjectManager::getInstance()
            ->get('Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable')
                ->getParentIdsByChild($product->getId());
        if (isset($parentProduct[0])) {
             //this is parent product id..
             $parentProduct = $this->productFactory->create()->load($parentProduct[0]);
        } else {
            $parentProduct = null;
        }

        foreach ($productAttributes as $productAttribute) {
            /** @var \Magento\Eav\Model\Entity\Attribute $attribute */

            //\Zend_Debug::dump($productAttribute);
            $attribute = $this->attributeFactory
                ->create()
                ->loadByCode('catalog_product', $productAttribute);



            if ($attribute->getSourceModel()) {
                $value = $attribute->getSource()->getOptionText($product->getData($productAttribute));
            } elseif (($attribute->getFrontendInput() === 'select')) {
                $value = $product->getAttributeText($productAttribute);
            } else {
                $value = $product->getData($productAttribute);
            }
            //\Zend_Debug::dump($value);


            //\Zend_Debug::dump((boolean)$value);
            //\Zend_Debug::dump((boolean)$parentProduct);
            // if no value of attribute try to get it from parent (if exists)
            if (!$value && $parentProduct) {
                if ($attribute->getSourceModel()) {
                    $value = $attribute->getSource()->getOptionText($parentProduct->getData($productAttribute));
                } elseif (($attribute->getFrontendInput() === 'select')) {
                    $value = $parentProduct->getAttributeText($productAttribute);
                } else {
                    $value = $parentProduct->getData($productAttribute);
                }
            }
            //\Zend_Debug::dump($value);

            $item->setData('product_' . $productAttribute, $value);
        }
        //die;

    }

    /**
     * @return array
     */
    protected function getProductAttributes()
    {
        $productAttributes = [];
        $config = $this->helper->getColumnConfig();
        if ($config) {
            $config = json_decode($config, true);
            foreach ($config as $column) {
                if (strpos($column['columntype'], 'product/') !== false) {
                    $productAttributes[] = str_replace('product/', '', $column['columntype']);
                }
            }
        }
        return $productAttributes;
    }

    /**
     * @param \Magento\Sales\Api\Data\OrderItemInterface $item
     * @param boolean                                    $ignore
     */
    protected function addItem(\Magento\Sales\Api\Data\OrderItemInterface $item, $ignore = false)
    {
        $this->prepareItem($item);
        $options = [];
        $productOptions = $item->getProductOptions();
        $arrayKeys = ['options', 'additional_options', 'attributes_info'];
        foreach ($arrayKeys as $key) {
            if (isset($productOptions[$key])) {
                $options = array_replace_recursive($options, $productOptions[$key]);
            }
        }
        if (!empty($productOptions)) {
            $productOptions = md5(json_encode($options));
        } else {
            $productOptions = false;
        }

        if ($item->getProduct()) {
            if (!array_key_exists($item->getProduct()->getId() . $productOptions, $this->items) || $ignore) {
                $this->items[$item->getProduct()->getId() . $productOptions] = $item;
            } else {
                $existingItem = $this->items[$item->getProduct()->getId() . $productOptions];
                $existingItem->setQtyOrdered($existingItem->getQtyOrdered() + $item->getQtyOrdered());
            }
        } else {
            $this->items[$item->getProductId() . $productOptions] = $item;
        }
    }

    /**
     * @return string
     */
    public function getLogoBlock()
    {
        $block = $this->getLayout()->createBlock(
            '\Fooman\PdfCore\Block\Pdf\Template\Logo',
            'pdfpickinglist.logo' . uniqid(),
            ['data' => ['storeId' => $this->getStoreIdFromFirstCollectionItem()]]
        );
        return $block->toHtml();
    }

    /**
     * @param array $styling
     *
     * @return mixed
     */
    public function getItemsBlock($styling = [])
    {
        $block = $this->getLayout()->createBlock(
            '\Fooman\PdfPickingList\Block\Table',
            'pdfpickinglist.items' . uniqid(),
            ['data' => [
                'tableColumns' => $this->getTableColumns()]
            ]
        );
        $block->setStyling($styling);
        $block->setCollection($this->getVisibleItems());
        return $block->toHtml();
    }

    private function getStoreIdFromFirstCollectionItem()
    {
        foreach ($this->getOrderCollection() as $order) {
            return $order->getStoreId();
        }
    }

    /**
     * @return boolean
     */
    public function isLogoOnRight()
    {
        return true;
    }

    /**
     * @return array
     */
    public function getTemplateVars()
    {
        return array_merge(
            parent::getTemplateVars(),
            ['order_collection' => $this->getOrderCollection()]
        );
    }

    protected function getTemplateText()
    {
        $templateVars = array_keys($this->getTemplateVars());
        $templateText = '{{layout handle="' . static::LAYOUT_HANDLE . '"';
        foreach ($templateVars as $var) {
            $templateText .= ' ' . $var . '=$' . $var;
        }
        $templateText .= '}}';
        return $templateText;
    }

    /**
     * @return array
     */
    public function getTableColumns()
    {
        $return = [];
        $config = $this->helper->getColumnConfig();

        if ($config) {
            $config = json_decode($config, true);
            foreach ($config as $column) {
                if (isset($column['width']) && $column['width'] > 0) {
                    $return[] = [
                        'index' => $column['columntype'],
                        'width' => $column['width'],
                        'title' => isset($column['title']) ? $column['title'] : ''
                    ];
                } else {
                    $return[] = [
                        'index' => $column['columntype'],
                        'title' => isset($column['title']) ? $column['title'] : ''
                    ];
                }
            }
        }
        return $return;
    }


    /**
     * @return string
     */
    public function getDefaultItemStyling()
    {
        return [
            'header' => [
                'default' => 'border-bottom:1px solid black;',
                'first'   => 'border-bottom:1px solid black;',
                'last'    => 'border-bottom:1px solid black;'
            ],
            'row'    => [
                'default' => 'border-bottom:0px none transparent;',
                'last'    => 'border-bottom:0px solid black;',
                'first'   => 'border-bottom:0px none transparent;'
            ],
            'table'  => ['default' => 'padding: 2px 0px;']
        ];
    }

    /**
     * @return string
     */
    public function getTitle()
    {
        return $this->helper->getTitle();
    }

    public function getForcedPageOrientation()
    {
        return $this->helper->getPageOrientation();
    }
}
