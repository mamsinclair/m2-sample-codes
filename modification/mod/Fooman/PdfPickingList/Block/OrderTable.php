<?php

namespace Fooman\PdfPickingList\Block;

use Fooman\PdfPickingList\Block\PdfCore\OrderItemListFactory;
use Magento\Framework\View\Element\Template\Context;
use Magento\Sales\Model\ResourceModel\Order\CollectionFactory;
use Fooman\PdfPickingList\Helper\Pdf as PdfConfig;

class OrderTable extends \Fooman\PdfCore\Block\Pdf\Table
{
    protected $_template = 'Fooman_PdfPickingList::pdf/table.phtml';
    /**
     * @var OrderItemListFactory
     */
    private $pdfOrderItemListFactory;

    /**
     * @var CollectionFactory
     */
    private $collectionFactory;

    /**
     * @var PdfConfig
     */
    private $pdfConfig;

    public function __construct(
        Context $context,
        OrderItemListFactory $pdfOrderItemListFactory,
        CollectionFactory $orderCollectionFactory,
        PdfConfig $pdfConfig,
        array $data = []
    ) {
        $this->pdfOrderItemListFactory = $pdfOrderItemListFactory;
        $this->collectionFactory = $orderCollectionFactory;
        $this->pdfConfig = $pdfConfig;
        parent::__construct($context, $data);

        $this->setStyling([
            'row' => [
                'first' => 'font-size: 16pt; font-weigth: bold;',
            ]
        ]);
    }

    public function getColumns()
    {
        if (is_null($this->columns)) {
            $this->columns = [];
            $i = 0;
            foreach ($this->tableColumns as $tableColumn) {
                if (isset($this->columnsMap[$tableColumn['index']])) {
                    throw new \Magento\Framework\Exception\LocalizedException(
                        __('Each column type can only appear once.')
                    );
                }
                $blockClass = sprintf(
                    '%s\%s',
                    'Fooman\PdfPickingList\Block\PdfCore\Column\Order',
                    \Magento\Framework\Api\SimpleDataObjectConverter::snakeCaseToUpperCamelCase($tableColumn['index'])
                );

                $index = $tableColumn['index'];

                $block = $this->getLayout()->createBlock($blockClass);

                $block->setId($tableColumn['index'])->setIndex($index);
                if (isset($tableColumn['width'])) {
                    $block->setWidthAbs($tableColumn['width']);
                }
                $this->setCustomRenderer($block);
                if ($this->getCurrencyCode()) {
                    $block->setCurrencyCode($this->getCurrencyCode());
                }

                if (isset($tableColumn['title']) && strlen($tableColumn['title']) >= 1) {
                    $block->setTitle($tableColumn['title']);
                }

                $this->columns[$i] = $block;
                $this->columnsMap[$tableColumn['index']] = $i;
                $i++;
            }
        }
        return $this->columns;
    }

    public function getAlign($isFirst, $isLast)
    {
        return 'left';
    }

    public function hasExtras(\Magento\Framework\DataObject $item)
    {
        return $this->pdfConfig->getOrderItemDetails();
    }

    /**
     * call subtable for order
     * @param  \Magento\Framework\DataObject $order [[Description]]
     * @return [[Type]] [[Description]]
     */
    public function getExtras(\Magento\Framework\DataObject $order)
    {
        $collection = $this->collectionFactory->create();
        $collection->addFieldToFilter('entity_id', $order->getEntityId());
        $pickingList = $this->pdfOrderItemListFactory->create(
            ['data' => ['order_collection' => $collection]]
        );
        $this->setStyling([
            'row' => [
                'first' => 'font-size: 16pt; font-weigth: bold;',
            ]
        ]);
        return $pickingList->getItemsBlock($this->getItemsStyle($order));

    }



    public function getItemsStyle(\Magento\Framework\DataObject $order) {
        /*
        switch ($order->getShippingDescription()) {
            case 'USA Standard Delivery (5-10 Working Days)':
                //green
                $bgcolor = '#a9d08e';
                $evencolor = '#84bb5d';
                break;
            case 'UK Next Day Delivery (Orders Placed Before 1pm)':
                //tan
                $bgcolor = '#ffcc99';
                $evencolor = '#ffa64d';
                break;
            case 'Germany Standard Delivery (5-10 Working Days)':
                //yellow
                $bgcolor = '#ffff99';
                $evencolor = '#ffff4d';
                break;
            case 'Electronic Gift Voucher':
                //blue
                $bgcolor = '#3366ff';
                $evencolor = '#0040ff';
                break;
            case 'Czech Republic Standard Delivery (5-10 Working Days)':
            case 'Europe Standard Delivery (5-10 Working Days)':
                //lavender
                $bgcolor = '#cc99ff';
                $evencolor = '#9933ff';
                break;
            default:
                //white
                $bgcolor = '#ffffff';
                $evencolor = '#f0f0f0';
                break;
        }
        /**/

        if (strpos($order->getShippingDescription(), 'USA Standard Delivery (5-10 Working Days)') !== false) {
            //green
            $bgcolor = '#a9d08e';
            $evencolor = '#84bb5d';
        } else if (strpos($order->getShippingDescription(), 'UK Next Day Delivery (Orders Placed Before 1pm)') !== false) {
            //tan
            $bgcolor = '#ffcc99';
            $evencolor = '#ffa64d';
        } else if (strpos($order->getShippingDescription(),  'Germany Standard Delivery (5-10 Working Days)') !== false) {
            //yellow
            $bgcolor = '#ffff99';
            $evencolor = '#ffff4d';
        } else if (strpos($order->getShippingDescription(), 'Electronic Gift Voucher') !== false) {
            //blue
            $bgcolor = '#3366ff';
            $evencolor = '#0040ff';
        } else if (strpos($order->getShippingDescription(), 'Czech Republic Standard Delivery (5-10 Working Days)') !== false) {
            //lavender
            $bgcolor = '#cc99ff';
            $evencolor = '#9933ff';
        } else if (strpos($order->getShippingDescription(), 'Europe Standard Delivery (5-10 Working Days)') !== false) {
            //lavender
            $bgcolor = '#cc99ff';
            $evencolor = '#9933ff';
        } else {
            //white
            $bgcolor = '#ffffff';
            $evencolor = '#f0f0f0';
        }

        $this->setSyling([
            'header' => [
                'default' => 'border-top:0px solid transparent;',
                'first'   => 'border-top:2px solid black;',
                'last'    => 'border-top:2px solid black;'
            ],
            'row'    => [
                'default' => 'border-bottom:0px none transparent; font-size: 14pt; font-weight: bold;',
                'last'    => 'border-bottom:0px solid black;',
                'first'   => 'border-bottom:0px none transparent; font-size: 14pt; font-weight: bold;'
            ],
            'table'  => ['default' => 'padding: 2px 0px;']
        ]);

        return [
            'header' => [
                'default' => 'font-size: 10pt; font-weight: normal; border-bottom:1px solid black; text-align: left;',
                'first'   => 'font-size: 10pt; font-weight: normal; border-bottom:1px solid black; text-align: left;',
                'last'    => 'font-size: 10pt; font-weight: normal; border-bottom:1px solid black; text-align: left;'
            ],
            'row'    => [
                'default' => 'font-size: 10pt; font-weight: normal; border-bottom:0px none transparent; padding-top: 3px; padding-bottom: 3px; text-align: left;',
                'last'    => 'font-size: 10pt; font-weight: normal; border-bottom:0px none transparent; padding-top: 3px; padding-bottom: 3px; text-align: left;',
                'first'   => 'font-size: 10pt; font-weight: normal; border-bottom:0px none transparent; padding-top: 6px; padding-bottom: 3px; text-align: left;',
                'even'    => $evencolor,
            ],
            'table'  => ['default' => 'padding: 10px; background-color: '.$bgcolor]
        ];
    }

}
