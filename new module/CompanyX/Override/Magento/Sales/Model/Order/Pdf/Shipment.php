<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shopcreator\CompanyX\Override\Magento\Sales\Model\Order\Pdf;

/**
 * Sales Order Shipment PDF model
 * @SuppressWarnings(PHPMD.CouplingBetweenObjects)
 */
class Shipment extends \Magento\Sales\Model\Order\Pdf\Shipment
{
    protected $_style;
    public $order;
    public $firstPage;
    public $lastPage;
    public $itemsPrice;
    public $shippingCost;
    public $orderPages = [];

    /**
     * @var \Magento\SalesRule\Api\Data\CouponInterfaceFactory
     */
    protected $_couponFactory;
    /**
     * @var \Magento\SalesRule\Api\RuleRepositoryInterface
     */
    protected $_ruleRepositoryInterface;


    /**
     * @param \Magento\Payment\Helper\Data $paymentData
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig
     * @param \Magento\Framework\Filesystem $filesystem
     * @param Config $pdfConfig
     * @param \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory
     * @param \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory
     * @param \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate
     * @param \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation
     * @param \Magento\Sales\Model\Order\Address\Renderer $addressRenderer
     * @param \Magento\Store\Model\StoreManagerInterface $storeManager
     * @param \Magento\Framework\Locale\ResolverInterface $localeResolver
     * @param array $data
     *
     * @SuppressWarnings(PHPMD.ExcessiveParameterList)
     */
    public function __construct(
        \Magento\Payment\Helper\Data $paymentData,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\App\Config\ScopeConfigInterface $scopeConfig,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Sales\Model\Order\Pdf\Config $pdfConfig,
        \Magento\Sales\Model\Order\Pdf\Total\Factory $pdfTotalFactory,
        \Magento\Sales\Model\Order\Pdf\ItemsFactory $pdfItemsFactory,
        \Magento\Framework\Stdlib\DateTime\TimezoneInterface $localeDate,
        \Magento\Framework\Translate\Inline\StateInterface $inlineTranslation,
        \Magento\Sales\Model\Order\Address\Renderer $addressRenderer,
        \Magento\Store\Model\StoreManagerInterface $storeManager,
        \Magento\Framework\Locale\ResolverInterface $localeResolver,
        \Magento\SalesRule\Api\Data\CouponInterfaceFactory $couponFactory,
        \Magento\SalesRule\Api\RuleRepositoryInterface $ruleRepositoryInterface,
        array $data = []
    ) {
        parent::__construct(
            $paymentData,
            $string,
            $scopeConfig,
            $filesystem,
            $pdfConfig,
            $pdfTotalFactory,
            $pdfItemsFactory,
            $localeDate,
            $inlineTranslation,
            $addressRenderer,
            $storeManager,
            $localeResolver,
            $data
        );

        $this->_couponFactory = $couponFactory;
        $this->_ruleRepositoryInterface = $ruleRepositoryInterface;
    }


    /**
     * Return PDF document
     *
     * @param \Magento\Sales\Model\Order\Shipment[] $shipments
     * @return \Zend_Pdf
     */
    public function getPdf($shipments = [])
    {
        $this->_beforeGetPdf();
        $this->_initRenderer('shipment');

        $pdf = new \Zend_Pdf();
        $this->_setPdf($pdf);
        $this->_style = new \Zend_Pdf_Style();
        //$this->_resetFont($this->_style);
        foreach ($shipments as $shipment) {
            $this->itemsPrice = 0;
            $this->shippingCost = 0;
            if ($shipment->getStoreId()) {
                $this->_localeResolver->emulate($shipment->getStoreId());
                $this->_storeManager->setCurrentStore($shipment->getStoreId());
            }

            $this->order = $shipment->getOrder();
            $this->orderPages[$this->order->getId()] = [];
            $page = $this->newPage(['return_address' => true]);
            $this->y = 331;
/**/
            $drawable_items = [];
            foreach ($shipment->getAllItems() as $item) {
                if ($item->getOrderItem()->getParentItem()) {
                    continue;
                }
                $drawable_items[] = $item;
            }

            $this->y = 332;
            $modulo = ((count($drawable_items) % 12 <= 8)
                ? 12
                : ((count($drawable_items) % 11 <= 8)
                    ? 11
                    : 10 ));
            $total_pages = ceil(count($drawable_items)/$modulo);
            for ($i = 0; $i < count($drawable_items); $i++) {
                $this->itemsPrice += ($drawable_items[$i]->getPrice() * $drawable_items[$i]->getQty());
                $this->_drawItem($drawable_items[$i], $page, $this->order);
                $page = end($pdf->pages);
                if ((($i+1) % $modulo) == 0 || $this->y < 140 ) {
                    //@TODO insert line, return codes
                    $this
                        ->__drawLineBelowItemListWithReasonCodes($page);
                    // open new one
                    $page = $this->newPage();
                    $this->y = 332;
                } else {
                    $this->y -=5;
                }
            }
/**/
            //die();
            //@TODO handle vouchers
            $hadVoucher = false;
            if ($code /*assignment*/= $this->order->getCouponCode()) {

                $rule = $this->_ruleRepositoryInterface
                    ->getById($this->_couponFactory->create()->load($code, 'code')->getRuleId());

                $this->y -= 15;
                $lines = [[]];

                $lines[0][] = ['text' => $code, 'feed' => 237];
                $lines[0][] = ['text' => $this->string->split($rule->getDescription(), 65, true, true), 'feed' => 342];
                $lines[0][] = ['text' => $this->order->formatPriceTxt($this->order->getDiscountAmount()), 'feed' => 602, 'align' => 'center', 'width' => 30];
                $lines[0][] = ['text' => 1, 'feed' => 648, 'align' => 'center', 'width' => 30];
                $lines[0][] = ['text' => $this->order->formatPriceTxt($this->order->getDiscountAmount()), 'feed' => 694, 'align' => 'center', 'width' => 30];

                $lineBlock = ['lines' => $lines, 'height' => 10];
                $this->drawLineBlocks($page, [$lineBlock]);
                $hadVoucher = true;
            }

            //@TODO handle shipping
            if (!$hadVoucher) {
                $this->y -= 15;
            }
            $lines = [[]];
            $lines[0][] = ['text' => __('Shipping & Handling'), 'feed' => 237];
            $lines[0][] = ['text' => $this->string->split($this->order->getShippingDescription(), 65, true, true), 'feed' => 342];
            $lines[0][] = ['text' => $this->order->formatPriceTxt($this->order->getShippingAmount()), 'feed' => 602, 'align' => 'center', 'width' => 30];
            $lines[0][] = ['text' => 1, 'feed' => 648, 'align' => 'center', 'width' => 30];
            $lines[0][] = ['text' => $this->order->formatPriceTxt($this->order->getShippingAmount()), 'feed' => 694, 'align' => 'center', 'width' => 30];

            $lineBlock = ['lines' => $lines, 'height' => 10];
            $this->drawLineBlocks($page, [$lineBlock]);

        $this
                // inserting page numbers
                ->__insertPageNumbers()
                // insert necessary things to last page
                ->__drawLineBelowItemListWithReasonCodes($this->lastPage)
                ->__insertReturnDescriptions($this->lastPage)
                ->__insertOrderTotalSection($this->lastPage)


                // insert return address into 1st page - do as last
                ->__drawReturnAddressWithOrderNumber($this->firstPage)
            ;

        }




        $this->_afterGetPdf();
        if ($shipment->getStoreId()) {
            $this->_localeResolver->revert();
        }
        return $pdf;
    }

    /**
     * Create new page and assign to PDF object
     *
     * @param  array $settings
     * @return \Zend_Pdf_Page
     */
    public function newPage(array $settings = []) {
        /* Add new table head */
        $page = $this->_getPdf()->newPage(\Zend_Pdf_Page::SIZE_A4_LANDSCAPE);
        $this->_getPdf()->pages[] = $page;
        $this->_resetFont($page);
        $this->y = 580;
        $this
            //->__drawReferenceLine($page) // DONE
            ->__insertLogo($page) // DONE
            ->__drawLineBelowLogo($page) // DONE
            ->__drawLineBelowItemsHeader($page)
            ->__drawLineVertical($page) // DONE
            ->__insertBillingAddress($page) // DONE
            ->__insertShippingAddress($page) // DONE
            ->__insertItemsHeaders($page)
            ->__insertOrderInfo($page); //done
        if (!empty($settings['return_address'])) {
            $this->firstPage = $page;
            //$this->__drawReturnAddressWithOrderNumber($page); // DONE, but need to consider
        }

        $this->lastPage = $page;
        $this->orderPages[$this->order->getId()][] = $page;

        return $page;
    }


    private function __drawReferenceLine(\Zend_Pdf_Page $page) {
        $page->setFillColor(new \Zend_Pdf_Color_Rgb(0.5, 0, 0));
        $page->drawLine(0, 140, 842, 140);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));
        return $this;
    }

    // all page - fix placement
    private function __insertLogo(\Zend_Pdf_Page $page) {
        $this->y = 595;
        $image = $this->_scopeConfig->getValue(
            'sales/identity/logo',
            \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
            $this->order->getStoreId()
        );
        if ($image) {
            $imagePath = '/sales/store/logo/' . $image;
            if ($this->_mediaDirectory->isFile($imagePath)) {
                $image = \Zend_Pdf_Image::imageWithPath($this->_mediaDirectory->getAbsolutePath($imagePath));
                $top = 580;
                //top border of the page
                $widthLimit = 158;
                //half of the page width
                $heightLimit = 945;
                //assuming the image is not a "skyscraper"
                $width = $image->getPixelWidth();
                $height = $image->getPixelHeight();

                //preserving aspect ratio (proportions)
                $ratio = $width / $height;
                if ($ratio > 1 && $width > $widthLimit) {
                    $width = $widthLimit;
                    $height = $width / $ratio;
                } elseif ($ratio < 1 && $height > $heightLimit) {
                    $height = $heightLimit;
                    $width = $height * $ratio;
                } elseif ($ratio == 1 && $height > $heightLimit) {
                    $height = $heightLimit;
                    $width = $widthLimit;
                }

                $y1 = $top - $height;
                $y2 = $top;
                $x1 = 510 - $width/2;
                $x2 = 510 + $width/2;

                //coordinates after transformation are rounded by Zend
                $page->drawImage($image, $x1, $y1, $x2, $y2);

                $this->y = $y1 - 10;
            }
        }
        return $this;
    }
    private function __drawLineBelowLogo(\Zend_Pdf_Page $page) {
        $page->drawLine(234, 540, 815, 540);
        return $this;
    }
    private function __drawLineBelowItemsHeader(\Zend_Pdf_Page $page) {
        $page->drawLine(234, 344, 815, 344);
        return $this;
    }
    private function __drawLineVertical(\Zend_Pdf_Page $page) {
        $page->drawLine(225, 580, 225, 30);
        return $this;
    }
    private function __insertBillingAddress(\Zend_Pdf_Page $page) {
        //\Zend_Debug::dump($page);
        $this->_setHeaderFont($page);
        $page->drawText(__('BILLING DETAILS'), 237, 525, 'UTF-8');
        $this->_resetFont($page);
        //$this->_setFontRegular($page, 8);
        $billingAddress = $this->_formatAddress($this->addressRenderer->format(
            $this->order->getBillingAddress(),
            'companyx_packing_slip'
        ));
        return $this->__renderAddress($page, $billingAddress, 515);
    }
    private function __insertShippingAddress(\Zend_Pdf_Page $page) {
        $this->_setHeaderFont($page);
        $page->drawText(__('SHIPPING DETAILS'), 237, 440, 'UTF-8');
        $this->_resetFont($page);
        //$this->_setFontRegular($page, 8);
        if (!$this->order->getIsVirtual()) {
            /* Shipping Address */
            $shippingAddress = $this->_formatAddress($this->addressRenderer->format(
                $this->order->getShippingAddress(),
                'companyx_packing_slip'
            ));
        } else {
            $shippingAddress = $this->_formatAddress($this->addressRenderer->format(
                $this->order->getBillingAddress(),
                'companyx_packing_slip'
            ));
        }
        return $this->__renderAddress($page, $shippingAddress, 430, false);
    }
    private function __renderAddress(\Zend_Pdf_Page $page, $address, $y, $is_billing = true) {
        $this->y = $y;
        $lines = [[]];
        $line = 0;
        foreach ($address as $value) {
            if ($value !== '') {
                foreach ($this->string->split($value, 80, true, true) as $_value) {
                    //$line[$line][] = ['text' => $item->getQty() * 1, 'feed' => 35]; $_value;

                    $lines[$line][] = [
                        'text' => strip_tags(ltrim($_value)),
                        'feed' => 237,
                        'font_size' => 8,
                        'align' => 'left',
                    ];
                    $line++;
                    //$page->drawText(strip_tags(ltrim($_value)), 237, $y, 'UTF-8');
                    //$y -= 12;
                }
                /*
                foreach ($text as $part) {
                    $page->drawText(strip_tags(ltrim($part)), 285, $this->y, 'UTF-8');
                    $this->y -= 15;
                }
                */
            }
        }


        $lineBlock = ['lines' => $lines, 'height' => 9];
        $this->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);

        return $this;
    }
    private function __insertOrderInfo(\Zend_Pdf_Page $page) {
        $this->y = 525;
        $lines = [[]];

        // draw SKU
        $lines[0][] = [
            'text' => __('INVOICE'),
            'feed' => 810,
            'font_size' => 12,
            'align' => 'right',
            'height' => 14
        ];
        $lines[1][] = [
            'text' => __('ORDER NUMBER').': #'.$this->order->getIncrementId(),
            'feed' => 810,
            'font_size' => 10,
            'align' => 'right',
        ];
        $lines[2][] = [
            'text' => __('ORDER DATE').': '.$this->_formatDate($this->order->getCreatedAt()),
            'feed' => 810,
            'font_size' => 10,
            'align' => 'right',
        ];
        $lines[3][] = [
            'text' => __('PAYMENT METHOD').': '.$this->order->getPayment()->getMethodInstance()->getTitle(),
            'feed' => 810,
            'font_size' => 10,
            'align' => 'right',
        ];


        $lineBlock = ['lines' => $lines, 'height' => 12];
        $this->drawLineBlocks($page, [$lineBlock], ['table_header' => true]);

        return $this;
    }
    private function __insertItemsHeaders(\Zend_Pdf_Page $page) {
        $this->_setHeaderFont($page);
        /* Add table head */
        $this->y = 360;
        //columns headers
        $lines[0][] = ['text' => __('PRODUCT CODE'), 'feed' => 237];
        $lines[0][] = ['text' => __('DESCRIPTION'), 'feed' => 342];
        $lines[0][] = ['text' => __('SIZE'), 'feed' => 557, 'align' => 'center', 'width' => 50];
        $lines[0][] = ['text' => [__('UNIT'), __('PRICE')], 'feed' => 602, 'align' => 'center', 'width' => 30];
        $lines[0][] = ['text' => __('UNITS'), 'feed' => 648, 'align' => 'center', 'width' => 30];
        $lines[0][] = ['text' => __('TOTAL'), 'feed' => 694, 'align' => 'center', 'width' => 30];
        $lines[0][] = ['text' => [__('RETURN'), __('REASON CODE')], 'feed' => 747, 'align' => 'center', 'width' => 60];

        $lineBlock = ['lines' => $lines, 'height' => 10];
        $this->drawLineBlocks($page, [$lineBlock]);
        $this->_resetFont($page);
        return $this;
    }

    // all page - dynamic placement
    private function __drawLineBelowItemListWithReasonCodes(\Zend_Pdf_Page $page) {
        $this->_resetFont($page, 9);
        $page->drawLine(234, $this->y, 815, $this->y);
        $this->y -= 10;
        $page->drawText(
                        trim(strip_tags(__('REASON CODES - (1) Too Small, (2) Too Large, (3) Didn\'t Suit, (4) Ordered Multiple For Choice, (5) Ordered Multiple Sizes, (6) Not As Described, (7) Faulty'))),
                        237,
                        $this->y,
                        'UTF-8'
                    );
        $this->y -= 10;
        return $this;

    }

    // 1st page only - fix placement
    private function __drawReturnAddressWithOrderNumber(\Zend_Pdf_Page $page) {
        $safetY = $this->y;
        // Order number - DONE
        //$this->_resetFont($page);

        $this->_resetFont($page);
        $this->y = 335;
        $lines = [[]];

        $lines[0][] = ['text' =>'#'.$this->order->getIncrementId(), 'feed' => 118, 'align' => 'center'];
        $lines[1][] = ['text' => '(#'.$this->order->getId().')', 'feed' => 118, 'align' => 'center'];

        $lineBlock = ['lines' => $lines, 'height' => 10];
        $this->drawLineBlocks($page, [$lineBlock]);


         // return address
        $this->_setReturnAddressHeaderFont($page);
        //$page->rotate(421, 398, deg2rad(-90));
        $page->rotate(0, 595, deg2rad(-90));
        /**
        $page
            ->drawText('SANYI A LO', 300, 300, 'UTF-8');
        /**/
        //$this->y = 150;
        $this->y = 775;
        $lines = [[]];

        $i = 0;
        foreach (explode(
                     "\n",
                     $this->_scopeConfig->getValue(
                         'sales/identity/address',
                         \Magento\Store\Model\ScopeInterface::SCOPE_STORE,
                         $this->order->getStoreId()
                     )
                 ) as $value) {

            if ($value !== '') {
                $value = preg_replace('/<br[^>]*>/i', "\n", $value);
                foreach ($this->string->split($value, 45, true, true) as $_value) {
                    //$lines[$i][] = ['text' => $_value, 'feed' => 290, 'font_size' => 14, 'height' => ($i == 0 ?22:17)];
                    $lines[$i][] = ['text' => $_value, 'feed' => 71, 'font_size' => 14, 'height' => ($i == 0 ?22:17)];
                    $i++;
                    if ($i == 1) {
                        $this->_setReturnAddressFont($page);
                    }
                }
            }
        }

        $lineBlock = ['lines' => $lines, 'height' => 10];
        $this->drawLineBlocks($page, [$lineBlock]);
        /**/
        //$page->drawCircle(520, 400, 2);
        //$page->rotate(480, 400, deg2rad(90));
        $page->rotate(0, 595, deg2rad(90));

        $this->y = $safetY;
        return $this;
    }

    //last page only - dynamic placement
    private function __insertReturnDescriptions(\Zend_Pdf_Page $page) {
        $safetY = $this->y;
        $this->_resetFont($page, 7);

        $return_description = __('Should any item(s) from your order be unsuitable, simply return them to us by completing the return boxes on the form above. Tick the item(s) that you are returning and write in the return reason code alongside this. Item(s) being returned should be in their original condition, re-packaged and posted using the returns sticker attached. Postal charges are at the cost of the customer; we recommend using a recorded postal method and retaining your receipt for this. Once your refund has been processed you will receive an email confirming this, please allow up to 21 days from the day the parcel is sent for this to take place. If  you are returning a faulty or incorrect item, please contact customer services via your online account before sending. Full details can be found on our website.');
        /*
        $page->drawText(
            __('RETURNS'),
            237,
            $y,
            'UTF-8'
        );
        $page->drawText(
            $this->string->split($return_description, 111, true, true)
            237,
            $y-16,
            'UTF-8'
        );
        */
        $this->y -= 8;
        $lines = [[]];

        $lines[0][] = ['text' => __('RETURNS'), 'feed' => 237, 'height' => '14'];
        $line = 1;
        foreach ($this->string->split($return_description, 102, true, true) as $_value)  {
            $lines[$line][] = ['text' => $_value, 'feed' => 237 ];
            $line++;
        }

        $lineBlock = ['lines' => $lines, 'height' => 10];
        $this->drawLineBlocks($page, [$lineBlock]);

        $this->y = $safetY;
        return $this;
    }
    private function __insertOrderTotalSection(\Zend_Pdf_Page $page) {
        $safetY = $this->y;
        $this->y -= 10;
        $this->_resetFont($page);

        $page->setFillColor(new \Zend_Pdf_Color_Rgb(0.6553, 0.5843, 0.5216));
        $page->setLineWidth(0.001);
        $page->drawRectangle(660, $this->y-18, 815, $this->y-33, \Zend_Pdf_Page::SHAPE_DRAW_FILL);
        $page->drawRectangle(660, $this->y-54, 815, $this->y-69, \Zend_Pdf_Page::SHAPE_DRAW_FILL);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));

        $page->setLineWidth(1);

        $page->drawLine(660, $this->y-3, 815, $this->y-3);
        $page->drawLine(660, $this->y-18, 815, $this->y-18);
        $page->drawLine(660, $this->y-33, 815, $this->y-33);
        $page->drawLine(660, $this->y-54, 815, $this->y-54);
        $page->drawLine(660, $this->y-69, 815, $this->y-69);


        $tax_info = $this->order->getFullTaxInfo();
        //\Zend_Debug::dump($tax_info);
        $tax_rate = $tax_info[0]['percent']
            ?
            : (float) $this->order->getAllItems()[0]->getTaxPercent();
        /*foreach ($this->order->getAllItems() as $item) {
            \Zend_Debug::dump($item->getTaxPercent());
        }
        die;/**/
        $grand_total = $this->itemsPrice + $this->order->getShippingAmount() + $this->order->getDiscountAmount();
        $tax_amount = $grand_total * $tax_rate/ 100;
        $net_total = $grand_total - $tax_amount;
        $lines = [[]];

        $lines[0][] = ['text' => __('VAT DETAILS'), 'feed' => 663, 'height' => '15'];

        $lines[1][] = ['text' => __('NET'), 'feed' => 663, 'height' => '14', 'font_size' => 8];
        $lines[1][] = ['text' => __('VAT RATE'), 'feed' => 722, 'height' => '14', 'font_size' => 8];
        $lines[1][] = ['text' => __('TOTAL'), 'feed' => 812, 'height' => '14', 'align' => 'right', 'font_size' => 8];

        $lines[2][] = ['text' => $this->order->formatPriceTxt($net_total), 'feed' => 663, 'height' => '21'];
        $lines[2][] = ['text' => $tax_rate.'%', 'feed' => 722, 'height' => '21'];
        $lines[2][] = ['text' => $this->order->formatPriceTxt($tax_amount), 'feed' => 812, 'height' => '21', 'align' => 'right'];

        $lines[3][] = ['text' => __('GRAND TOTAL'), 'feed' => 663, 'height' => '15'];

        $lines[4][] = ['text' => __('FINAL AMOUNT INC VAT'), 'feed' => 663, 'height' => '15', 'font_size' => 8];
        $lines[4][] = ['text' => $this->order->formatPriceTxt($grand_total), 'feed' => 812, 'height' => '15', 'align' => 'right'];

        $lineBlock = ['lines' => $lines, 'height' => 10];
        $this->drawLineBlocks($page, [$lineBlock]);

        $this->y = $safetY;
        return $this;
    }




    protected function _resetFont(\Zend_Pdf_Page $page, $size = 9) {
        $this->_setFontRegular($page, $size);
        return $this;
    }
    protected function _setHeaderFont(\Zend_Pdf_Page $page, $size = 11) {
        $this->_setFontRegular($page, $size);
        return $this;
    }
    protected function _setReturnAddressHeaderFont(\Zend_Pdf_Page $page, $size = 18) {
        //$this->_setFontRegular($page, 12);
        $this->_setFontBold($page, $size);
        return $this;
    }
    protected function _setReturnAddressFont(\Zend_Pdf_Page $page, $size = 18) {
        $this->_setFontRegular($page, $size);
        //$this->_setFontBold($page, 20);
        return $this;
    }
    public function _formatDate(
        $date = null,
        $format = \IntlDateFormatter::SHORT,
        $showTime = false,
        $timezone = null
    ) {
        $date = $date instanceof \DateTimeInterface ? $date : new \DateTime($date);
        return $this->_localeDate->formatDateTime(
            $date,
            $format,
            $showTime ? $format : \IntlDateFormatter::NONE,
            null,
            $timezone
        );
    }

    protected function __insertPageNumbers() {
        $safetY = $this->y;
        $count = count($this->orderPages[$this->order->getId()]);
        $current = 1;
        foreach ($this->orderPages[$this->order->getId()] as $page) {
            $this->y = 40;
            $this->_resetFont($page);
            $lines = [[]];

            $lines[0][] = ['text' => __('Page %1/%2', $current, $count), 'feed' => 421, 'align' => 'center', 'height' => '11', 'width' => 180];
            $lines[1][] = ['text' => __('www.companyxonline.com'), 'feed' => 421, 'align' => 'center', 'height' => '11', 'width' => 180];

            $lineBlock = ['lines' => $lines, 'height' => 10];
            $this->drawLineBlocks($page, [$lineBlock]);
            $current++;
        }

        $this->y = $safetY;

        return $this;
    }



    //overwrites of parent
    /**
     * Format address
     *
     * @param  string $address
     * @return array
     */
    protected function _formatAddress($address)
    {
        $return = [];
        foreach (explode('|', $address) as $str) {
            foreach ($this->string->split($str, 80, true, true) as $part) {
                if (empty($part)) {
                    continue;
                }
                $return[] = $part;
            }
        }
        return $return;
    }

    /**
     * Draw Item process
     *
     * @param  \Magento\Framework\DataObject $item
     * @param  \Zend_Pdf_Page $page
     * @param  \Magento\Sales\Model\Order $order
     * @return \Zend_Pdf_Page
     */
    protected function _drawItem(\Magento\Framework\DataObject $item, \Zend_Pdf_Page $page, \Magento\Sales\Model\Order $order)
    {
        $type = $item->getOrderItem()->getProductType();
        $renderer = $this->_getRenderer($type);
        $renderer->setOrder($order);
        $renderer->setItem($item);
        $renderer->setPdf($this);
        $renderer->setPage($page);
        $renderer->setCoorY($this->y);
        $renderer->setRenderedModel($this);

        $renderer->draw();

        return $renderer->getPage();
    }
}
