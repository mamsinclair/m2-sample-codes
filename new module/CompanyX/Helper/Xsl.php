<?php

/**
 * Product:       Xtento_ProductExport (2.3.4)
 * ID:            tiT9BYYH1SIBbwDgE31EfEfgbeA0YGxPsIYuGrYW90A=
 * Packaged:      2017-07-19T13:09:50+00:00
 * Last Modified: 2017-05-24T11:40:58+00:00
 * File:          app/code/Xtento/ProductExport/Helper/Xsl.php
 * Copyright:     Copyright (c) 2017 XTENTO GmbH & Co. KG <info@xtento.com> / All rights reserved.
 */

namespace Shopcreator\CompanyX\Helper;

use Magento\Framework\App\ObjectManager;
use Magento\Framework\App\Config\ScopeConfigInterface;

class Xsl extends \Magento\Framework\App\Helper\AbstractHelper
{
    public static function getPriceBySku($sku, $store)
    {
        // Needs to use the object manager as this is a static function (which is required for XSL)
        /** @var \Magento\Catalog\Api\Data\ProductInterface $product */
        /** @var \Magento\Catalog\Model\Product $product */
        $product = \Magento\Framework\App\ObjectManager::getInstance()
            ->create('\Magento\Catalog\Api\ProductRepositoryInterface')
                ->get($sku, false, $store, true);
/*
\Magento\Framework\App\ObjectManager::getInstance()
->create('\Psr\Log\LoggerInterface')
->debug("child price: ".var_export($product->getTypeId(), true)."", ['method' => __METHOD__]);
/**/
        if ($product->getTypeId() == 'configurable') {
            $price = 0;
            foreach ($product->getTypeInstance()->getUsedProducts($product) as $child){
/*
\Magento\Framework\App\ObjectManager::getInstance()
->create('\Psr\Log\LoggerInterface')
->debug("child price: ".var_export($child->getPrice(), true)."", ['method' => __METHOD__]);
/**/
                if ($price == 0) {
                    $price = $child->getPrice();
                } else if ($price > $child->getPrice()) {
                    $price = $child->getPrice();
                    }
                }
            } else {
            $price = $product->getPrice();
        }
        return $price;
    }

    public static function getStockStatus($sku, $store)
    {
        // Needs to use the object manager as this is a static function (which is required for XSL)
        /** @var \Magento\Catalog\Api\Data\ProductInterface $product */
        /** @var \Magento\Catalog\Model\Product $product */
        $product = \Magento\Framework\App\ObjectManager::getInstance()
            ->create('\Magento\Catalog\Api\ProductRepositoryInterface')
                ->get($sku, false, $store, true);
/*
\Magento\Framework\App\ObjectManager::getInstance()
->create('\Psr\Log\LoggerInterface')
->debug("child price: ".var_export($product->getTypeId(), true)."", ['method' => __METHOD__]);
/**/
        if ($product->getTypeId() == 'configurable') {
            $stock = 0;
            foreach ($product->getTypeInstance()->getUsedProducts($product) as $child){
/*
\Magento\Framework\App\ObjectManager::getInstance()
->create('\Psr\Log\LoggerInterface')
->debug("child price: ".var_export($child->getPrice(), true)."", ['method' => __METHOD__]);
/**/
                $stock += \Magento\Framework\App\ObjectManager::getInstance()
                    ->create('\Magento\CatalogInventory\Model\Stock\Item')
                        ->load($child->getId(), 'product_id')
                            ->getQty();
            }
        } else {
            $stock = \Magento\Framework\App\ObjectManager::getInstance()
                    ->create('\Magento\CatalogInventory\Model\Stock\Item')
                        ->load($product->getId(), 'product_id')
                            ->getQty();
        }
        return (int)(boolean)$stock;
    }
}
