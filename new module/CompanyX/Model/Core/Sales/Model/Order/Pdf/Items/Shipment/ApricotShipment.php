<?php
/**
 * Copyright © 2016 Magento. All rights reserved.
 * See COPYING.txt for license details.
 */
namespace Shopcreator\CompanyX\Model\Core\Sales\Model\Order\Pdf\Items\Shipment;

/**
 * Sales Order Shipment Pdf default items renderer
 */
class CompanyXShipment extends \Magento\Sales\Model\Order\Pdf\Items\AbstractItems
{
    /**
     * Core string
     *
     * @var \Magento\Framework\Stdlib\StringUtils
     */
    protected $string;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Tax\Helper\Data $taxData
     * @param \Magento\Framework\Filesystem $filesystem
     * @param \Magento\Framework\Filter\FilterManager $filterManager
     * @param \Magento\Framework\Stdlib\StringUtils $string
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Magento\Tax\Helper\Data $taxData,
        \Magento\Framework\Filesystem $filesystem,
        \Magento\Framework\Filter\FilterManager $filterManager,
        \Magento\Framework\Stdlib\StringUtils $string,
        \Magento\Framework\Model\ResourceModel\AbstractResource $resource = null,
        \Magento\Framework\Data\Collection\AbstractDb $resourceCollection = null,
        array $data = []
    ) {
        $this->string = $string;
        parent::__construct(
            $context,
            $registry,
            $taxData,
            $filesystem,
            $filterManager,
            $resource,
            $resourceCollection,
            $data
        );
    }

    /**
     * Draw item line
     *
     * @return void
     */
    public function draw()
    {
        $item = $this->getItem();
        $pdf = $this->getPdf();
        $page = $this->getPage();
        $order = $item->getOrderItem()->getOrder();
        $y = $this->getCoorY();
        $lines = [[]];

        // draw SKU
        $sku = $item->getOrderItem()->getParentItem()
            ? $item->getOrderItem()->getParentItem()->getSku()
            : $this->getSku($item);
        $lines[0][] = [
            'text' => $this->string->split($sku, 25),
            'feed' => 237,
            //'align' => 'right',
        ];

        // draw Product name
        $product_name = $this->getSku($item) == $item->getName()
            ? $item->getOrderItem()->getName()
            : $item->getName();
        $lines[0][] = [
            'text' => $this->string->split($product_name, 60, true, true),
            'feed' => 342
        ];
        // draw Product Size
        $options = $item->getOrderItem()->getProductOptions();
        //\Zend_Debug::dump($options);
        if (isset($options['attributes_info']) && !empty($options['attributes_info'])) {
            foreach ($options['attributes_info'] as $option) {
                if ($option['label'] == 'Size') {
                    $lines[0][] = ['text' => $option['value'], 'feed' => 557, 'align' => 'center', 'width' => 50];
                    continue;
                }

            }
        }

        // draw QTY and prices
        //$prices = $this->getItemPricesForDisplay();
        $lines[0][] = ['text' => $item->getQty() * 1, 'feed' => 648, 'align' => 'center', 'width' => 30];
        $lines[0][] = ['text' => $order->formatPriceTxt($item->getPrice()), 'feed' => 602, 'align' => 'center', 'width' => 30];
        $lines[0][] = ['text' => $order->formatPriceTxt($item->getPrice() * $item->getQty()), 'feed' => 694, 'align' => 'center', 'width' => 30];

        // checkbox
        //$lines[0][] = ['text' => '▯', 'feed' => 747, 'align' => 'center', 'width' => 60];
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(1));
        $page->setLineWidth(1);
        $page->drawRectangle(770, $y+8, 784, $y-3);
        $page->setFillColor(new \Zend_Pdf_Color_GrayScale(0));



        // Custom options
        $options = $this->getItemOptions();
        //\Zend_Debug::dump($options);
        if (isset($options['options']) && !empty($options['options'])) {
            $i = 1;
            foreach ($options as $option) {
                $lines[$i][] = [
                    'text' => $this->string->split($this->filterManager->stripTags($option['label']), 70, true, true).': ',
                    'font' => 'italic',
                    'feed' => 110,
                ];

                // draw options value
                if ($option['value']) {
                    $printValue = isset(
                        $option['print_value']
                    ) ? $option['print_value'] : $this->filterManager->stripTags(
                        $option['value']
                    );
                    $lines[$i][] = ['text' => $this->string->split($printValue, 50, true, true), 'feed' => 115];
                }
                $i++;
            }
        }
        $lineBlock = ['lines' => $lines];

        $page = $pdf->drawLineBlocks($page, [$lineBlock]/*, ['table_header' => true]*/);
        $this->setPage($page);

        return count($lines);
    }
}
