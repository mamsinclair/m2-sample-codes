<?php

/* * * Update amasty vendor product table ** */
// Shopcreator\RetailerReporting\Model\SalesIndexer

namespace Shopcreator\RetailerReporting\Model;

class SalesIndexer
    implements \Magento\Framework\Indexer\ActionInterface,
               \Magento\Framework\Mview\ActionInterface
{

    const TABLE_INDEX = 'marketplace_saleslist_index';
    const TABLE_SALES = 'marketplace_saleslist';

    protected $_resourceConnection;

    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        $this->_resourceConnection = $resourceConnection;
    }

    public function executeFull() {
        $this->updateAll();
    }
    public function executeList(array $ids) {
        $this->updateAll();
    }
    public function executeRow($id) {
        $this->updateAll();
    }
    public function execute($ids) {
        $this->updateAll();
    }

    public function updateAll() {
        $vendorsData = $this->getAllVendorsData();
        foreach ($vendorsData as $vendorData) {
            $this
                ->clearIndexByVendor($vendorData['seller_id'])
                ->saveVendorIndex($vendorData);
        }
    }

    protected function saveVendorIndex($updateArr) {
        $connection = $this->_resourceConnection->getConnection();
        $connection->insert($this->_resourceConnection->getTableName(self::TABLE_INDEX), $updateArr);
    }

    protected function clearIndexByVendor($vendorId) {
        $connection = $this->_resourceConnection->getConnection();
        $where = ['seller_id = ?' => (int) $vendorId];
        $connection->delete($this->_resourceConnection->getTableName(self::TABLE_INDEX), $where);
        return $this;
    }

    protected function getAllVendorsData() {

        $connection = $this->_resourceConnection->getConnection();
        $select = $connection->select()
            ->from(
                ['main_table' => $this->_resourceConnection->getTableName(self::TABLE_SALES)],
                [
                    'seller_id' => 'main_table.seller_id',
                    'number_of_orders' => new \Zend_Db_Expr('COUNT(DISTINCT(magerealorder_id))'),
                    'number_of_products' => new \Zend_Db_Expr('SUM(magequantity)'),
                    'sales_value' => new \Zend_Db_Expr('SUM(total_amount)'),
                    //'average_order' => new \Zend_Db_Expr('(SUM(total_amount) / COUNT(DISTINCT(magerealorder_id)))')
                ]
            )
            ->group('main_table.seller_id')
            ->where('main_table.seller_id > ?', 0);

        return $connection->fetchAll($select);
    }

}
