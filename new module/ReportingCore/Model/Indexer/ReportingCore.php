<?php
/**
 * Indexer Class
 *
 * @category    Magento2 module
 * @package     Shopcreator_ReportingCore
 * @version     1.0.0.0
 * @author      Shopcreator
 * @author      Robert Szeker
 * @copyright   Copyright (c) 1998-2017 Shopcreator (https://www.shopcreator.com/)
 * @copyright   Copyright (c) 2014-2017 Simple Evolve Ltd.
 * @license     https://www.shopcreator.com/license.html
 */
namespace Shopcreator\ReportingCore\Model\Indexer;

class ReportingCore
    implements \Magento\Framework\Indexer\ActionInterface,
               \Magento\Framework\Mview\ActionInterface
{
    /**
     * @var \Shopcreator\ReportingCore\Model\Indexer\AbstractProcessor[]
     */
    protected $_processors;

    /**
     * ReportingCore constuctor
     * @param \Shopcreator\ReportingCore\Model\Indexer\AbstractProcessor[] $processors
     */
    public function __construct(
        array $processors = []
    ) {
        $this->_processors = $processors;
    }

    /**
     * {@inheritdoc}
     */
    public function executeFull() {
        $this->updateAll();
    }

    /**
     * {@inheritdoc}
     */
    public function executeList(array $ids) {
        $this->updateAll();
    }

    /**
     * {@inheritdoc}
     */
    public function executeRow($id) {
        $this->updateAll();
    }

    /**
     * {@inheritdoc}
     */
    public function execute($ids) {
        $this->updateAll();
    }

    /**
     * Run processors
     */
    public function updateAll() {
        foreach ($this->_processors as $processor) {
            if ($processor instanceof \Shopcreator\ReportingCore\Model\Indexer\AbstractProcessor) {
                $processor->updateAll();
            }
        }
    }
}
