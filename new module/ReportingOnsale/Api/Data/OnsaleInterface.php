<?php
/**
 * Onsale interface
 *
 * @category    Magento2 module
 * @package     Shopcreator_ReportingOnsale
 * @version     1.0.0.0
 * @author      Shopcreator
 * @author      Robert Szeker
 * @copyright   Copyright (c) 1998-2017 Shopcreator (https://www.shopcreator.com/)
 * @copyright   Copyright (c) 2014-2017 Simple Evolve Ltd.
 * @license     https://www.shopcreator.com/license.html
 */
namespace Shopcreator\ReportingOnsale\Api\Data;

interface OnsaleInterface {
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID            = 'entity_id';
    const PRODUCT_ID    = 'product_id';
    const STORE_ID      = 'store_id';
    const ORDER_ID      = 'order_id';
    const QTY           = 'qty';
    const PRICE         = 'price';
    const SPECIAL_PRICE = 'special_price';
    const SALE_VALU     = 'sale_value';

    /**
     * No route page id
     */
    const ENTITY_ID = 'entity_id';
    const NOROUTE_ENTITY_ID = 'entity_id';

    /**
     * Cache tag
     */
    const CACHE_TAG = 'report_onsale';

    /**
     *  Definition all the setters and getters we would use when interacting with our model
     */
    public function getId();
    public function setId($id);
}
