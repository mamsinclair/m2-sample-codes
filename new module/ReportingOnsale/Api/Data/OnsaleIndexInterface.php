<?php
/**
 * OnsaleIndexIndex interface
 *
 * @category    Magento2 module
 * @package     Shopcreator_ReportingOnsale
 * @version     1.0.0.0
 * @author      Shopcreator
 * @author      Robert Szeker
 * @copyright   Copyright (c) 1998-2017 Shopcreator (https://www.shopcreator.com/)
 * @copyright   Copyright (c) 2014-2017 Simple Evolve Ltd.
 * @license     https://www.shopcreator.com/license.html
 */
namespace Shopcreator\ReportingOnsale\Api\Data;

interface OnsaleIndexInterface {
    /**
     * Constants for keys of data array. Identical to the name of the getter in snake case
     */
    const ID                = 'entity_id';
    const PRODUCT_ID        = 'product_id';
    const NO_OF_ORDERS      = 'number_of_orders';
    const NO_OF_PRODUCTS    = 'number_of_products';
    const SALES_VALUE       = 'sales_value';

    /**
     * No route page id
     */
    const ENTITY_ID = 'entity_id';
    const NOROUTE_ENTITY_ID = 'entity_id';

    /**
     * Cache tag
     */
    const CACHE_TAG = 'report_onsale_index';

    /**
     *  Definition all the setters and getters we would use when interacting with our model
     */
    public function getId();
    public function setId($id);
}
