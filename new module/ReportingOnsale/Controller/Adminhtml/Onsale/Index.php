<?php
/**
 * Admin controller for onsale report
 *
 * @category    Magento2 module
 * @package     Shopcreator_ReportingOnsale
 * @version     1.0.0.0
 * @author      Shopcreator
 * @author      Robert Szeker
 * @copyright   Copyright (c) 1998-2017 Shopcreator (https://www.shopcreator.com/)
 * @copyright   Copyright (c) 2014-2017 Simple Evolve Ltd.
 * @license     https://www.shopcreator.com/license.html
 */
namespace Shopcreator\ReportingOnsale\Controller\Adminhtml\Onsale;

class Index extends \Magento\Backend\App\Action
{
    /**
     * Check for is allowed
     *
     * @return boolean
     */
    protected function _isAllowed()
    {
        return $this->_authorization->isAllowed(
            'Shopcreator_ReportingOnsale::onsale_report'
        );
    }

    /**
     * @return \Magento\Backend\Model\View\Result\Page
     */
    public function execute()
    {
        $resultPage = $this->resultFactory
            ->create(\Magento\Framework\Controller\ResultFactory::TYPE_PAGE);
        $resultPage
            ->setActiveMenu('Shopcreator_ReportingOnsale::onsale_report');
        $resultPage
            ->getConfig()
                ->getTitle()
                    ->prepend(__('On Sale Report'));

        return $resultPage;
    }
}
