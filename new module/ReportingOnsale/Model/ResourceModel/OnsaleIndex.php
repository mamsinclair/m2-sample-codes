<?php
/**
 * OnsaleIndex Resource model
 *
 * @category    Magento2 module
 * @package     Shopcreator_ReportingOnsale
 * @version     1.0.0.0
 * @author      Shopcreator
 * @author      Robert Szeker
 * @copyright   Copyright (c) 1998-2017 Shopcreator (https://www.shopcreator.com/)
 * @copyright   Copyright (c) 2014-2017 Simple Evolve Ltd.
 * @license     https://www.shopcreator.com/license.html
 */
namespace Shopcreator\ReportingOnsale\Model\ResourceModel;

class OnsaleIndex
    extends \Magento\Framework\Model\ResourceModel\Db\AbstractDb{
    /**
     * Store model
     *
     * @var null|\Magento\Store\Model\Store
     */
    protected $_store = null;

    /**
     * Construct
     *
     * @param \Magento\Framework\Model\ResourceModel\Db\Context $context
     * @param string $connectionName
     */
    public function __construct(
        \Magento\Framework\Model\ResourceModel\Db\Context $context,
        $connectionName = null
    ) {
        parent::__construct($context, $connectionName);
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('_shopcreator_reporting_onsale_index', 'entity_id');
    }
}
