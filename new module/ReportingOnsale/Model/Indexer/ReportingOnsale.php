<?php
/**
 * Onsale processor for Core Indexer Class
 *
 * @category    Magento2 module
 * @package     Shopcreator_ReportingOnsale
 * @version     1.0.0.0
 * @author      Shopcreator
 * @author      Robert Szeker
 * @copyright   Copyright (c) 1998-2017 Shopcreator (https://www.shopcreator.com/)
 * @copyright   Copyright (c) 2014-2017 Simple Evolve Ltd.
 * @license     https://www.shopcreator.com/license.html
 */
namespace Shopcreator\ReportingOnsale\Model\Indexer;

class ReportingOnsale
    extends \Shopcreator\ReportingCore\Model\Indexer\AbstractProcessor {

    /**
     * Tables
     */
    const TABLE_INDEX   = '_shopcreator_reporting_onsale_index';
    const TABLE_REPORT  = '_shopcreator_reporting_onsale';

    /**
     * {@inheritdoc}
     */
    public function __construct(
        \Magento\Framework\App\ResourceConnection $resourceConnection
    ) {
        parent::__construct($resourceConnection);
    }

    /**
     * {@inheritdoc}
     */
    public function updateAll() {
        $productsData = $this->_getAllProductsData();
        foreach ($productsData as $productData) {
            $this
                ->clearIndexByProductId($productData['product_id'])
                ->saveProductIndex($productData);
        }
    }

    /**
     * Save product index data
     *
     * @param array $updateArr
     * @return $this
     */
    protected function saveProductIndex($updateArr) {
        $connection = $this->_resourceConnection->getConnection();
        $connection->insert($this->_resourceConnection->getTableName(self::TABLE_INDEX), $updateArr);
    }

    /**
     * Clear index by product id
     *
     * @param  integer $productId
     * @return $this
     */
    protected function clearIndexByProductId($productId) {
        $connection = $this->_resourceConnection->getConnection();
        $where = ['product_id = ?' => (int) $productId];
        $connection->delete($this->_resourceConnection->getTableName(self::TABLE_INDEX), $where);
        return $this;
    }

    /**
     * Collect indexes
     *
     * @return collection[]
     */
    protected function _getAllProductsData() {
        $connection = $this->_resourceConnection->getConnection();
        $select = $connection->select()
            ->from(
                ['main_table' => $this->_resourceConnection->getTableName(self::TABLE_REPORT)],
                [
                    'product_id' => 'main_table.product_id',
                    'number_of_orders' => new \Zend_Db_Expr('COUNT(DISTINCT(order_id))'),
                    'number_of_products' => new \Zend_Db_Expr('SUM(qty)'),
                    'number_of_products_returned' => new \Zend_Db_Expr('SUM(qty_returned)'),
                    'sales_value' => new \Zend_Db_Expr('SUM(sale_value)'),
                    'returned_value' => new \Zend_Db_Expr('SUM(special_price * qty_returned)'),
                ]
            )
            ->group('main_table.product_id')
            ->where('main_table.product_id > ?', 0);

        return $connection->fetchAll($select);
    }
}
