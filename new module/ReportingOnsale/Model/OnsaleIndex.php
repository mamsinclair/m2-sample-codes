<?php
/**
 * OnsaleIndex model
 *
 * @category    Magento2 module
 * @package     Shopcreator_ReportingOnsale
 * @version     1.0.0.0
 * @author      Shopcreator
 * @author      Robert Szeker
 * @copyright   Copyright (c) 1998-2017 Shopcreator (https://www.shopcreator.com/)
 * @copyright   Copyright (c) 2014-2017 Simple Evolve Ltd.
 * @license     https://www.shopcreator.com/license.html
 */
namespace Shopcreator\ReportingOnsale\Model;

class OnsaleIndex
    extends \Magento\Framework\Model\AbstractModel
    implements \Shopcreator\ReportingOnsale\Api\Data\OnsaleIndexInterface
{
    /**
     * @var string
     */
    protected $_cacheTag = self::CACHE_TAG;

    /**
     * Prefix of model events names
     * @var string
     */
    protected $_eventPrefix = self::CACHE_TAG;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @param \Magento\Framework\Model\Context $context
     * @param \Magento\Framework\Registry $registry
     * @param \Magento\Framework\Model\ResourceModel\AbstractResource $resource
     * @param \Magento\Framework\Data\Collection\AbstractDb $resourceCollection
     * @param \Psr\Log\LoggerInterface $logger
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\Model\Context $context,
        \Magento\Framework\Registry $registry,
        \Shopcreator\ReportingOnsale\Model\ResourceModel\OnsaleIndex $resource,
        \Shopcreator\ReportingOnsale\Model\ResourceModel\OnsaleIndex\Collection $resourceCollection,
        \Psr\Log\LoggerInterface $logger,
        array $data = []
    ) {
        parent::__construct($context, $registry, $resource, $resourceCollection, $data);
        $this->_logger = $logger;
    }

    /**
     * Initialize resource model
     *
     * @return void
     */
    protected function _construct()
    {
        $this->_init('Shopcreator\ReportingOnsale\Model\ResourceModel\OnsaleIndex');
    }

    /**
     * Load object data
     *
     * @param int|null $id
     * @param string $field
     * @return $this
     */
    public function load($id, $field = null)
    {
        if ($id === null) {
            return $this->noRouteReport();
        }
        return parent::load($id, $field);
    }

    /**
     * Load No-Route model
     *
     * @return $this
     */
    public function noRouteReport()
    {
        return $this->load(self::NOROUTE_ENTITY_ID, $this->getIdFieldName());
    }


    /**
     * Get ID
     *
     * @return int
     */
    public function getId()
    {
        return parent::getData(self::ENTITY_ID);
    }

    /**
     * Set ID
     *
     * @param int $id
     * @return $this
     */
    public function setId($id)
    {
        return $this->setData(self::ENTITY_ID, $id);
    }
}
