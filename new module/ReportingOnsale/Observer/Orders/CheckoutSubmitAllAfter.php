<?php
/**
 * Observer for registering products sold with special price
 *
 * @category    Magento2 module
 * @package     Shopcreator_ReportingOnsale
 * @version     1.0.0.0
 * @author      Shopcreator
 * @author      Robert Szeker
 * @copyright   Copyright (c) 1998-2017 Shopcreator (https://www.shopcreator.com/)
 * @copyright   Copyright (c) 2014-2017 Simple Evolve Ltd.
 * @license     https://www.shopcreator.com/license.html
 */
namespace Shopcreator\ReportingOnsale\Observer\Orders;

class CheckoutSubmitAllAfter
    implements \Magento\Framework\Event\ObserverInterface {
    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var \\Shopcreator\ReportingOnsale\Api\Data\OnsaleInterfaceFactory
     */
    protected $_onsaleFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @param \Magento\Framework\Event\Manager            $eventManager
     * @param \Magento\Framework\ObjectManagerInterface   $objectManager
     * @param Magento\Customer\Model\Session              $customerSession
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Shopcreator\ReportingOnsale\Api\Data\OnsaleInterfaceFactory $onsaleFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_productRepository = $productRepository;
        $this->_onsaleFactory = $onsaleFactory;
        $this->_logger = $logger;
    }

    /**
     * Sales Order Place After event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        /** @var $orderInstance Order */
        $order = $observer->getOrder();

        foreach ($order->getAllItems() as $item) {
            $product = $this->_productRepository
                ->getById($item->getProduct()->getId());

            if (
                // special price set
                ($product->getSpecialPrice()
                 && $product->getPrice() > $product->getSpecialPrice())
                ||
                // catalog price rule
                ($product->getPrice() < $product->getFinalPrice())
            ) {
                $this->_onsaleFactory
                    ->create()
                        ->setProductId($item->getProduct()->getId())
                        ->setStoreId($order->getStoreId())
                        ->setOrderId($order->getId())
                        ->setItemId($item->getId())
                        ->setQty($item->getQtyOrdered())
                        ->setQtyReturned(0)
                        ->setPrice($product->getPrice())
                        ->setSpecialPrice($item->getPrice())
                        ->setSaleValue($item->getBaseRowTotal())
                        ->save();
            }
        }
    }
}
