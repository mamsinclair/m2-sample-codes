<?php
/**
 * Observer for registering products sold with special price and returned
 *
 * @category    Magento2 module
 * @package     Shopcreator_ReportingOnsale
 * @version     1.0.0.0
 * @author      Shopcreator
 * @author      Robert Szeker
 * @copyright   Copyright (c) 1998-2017 Shopcreator (https://www.shopcreator.com/)
 * @copyright   Copyright (c) 2014-2017 Simple Evolve Ltd.
 * @license     https://www.shopcreator.com/license.html
 */
namespace Shopcreator\ReportingOnsale\Observer\Orders;

class CreditmemoSaveAfter
    implements \Magento\Framework\Event\ObserverInterface {
    /**
     * @var \Shopcreator\ReportingOnsale\Api\Data\OnsaleInterfaceFactory
     */
    protected $_onsaleFactory;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /**
     * @param \Magento\Framework\Event\Manager            $eventManager
     * @param \Magento\Framework\ObjectManagerInterface   $objectManager
     * @param Magento\Customer\Model\Session              $customerSession
     * @param \Magento\Framework\Stdlib\DateTime\DateTime $date
     */
    public function __construct(
        \Shopcreator\ReportingOnsale\Api\Data\OnsaleInterfaceFactory $onsaleFactory,
        \Psr\Log\LoggerInterface $logger
    ) {
        $this->_onsaleFactory = $onsaleFactory;
        $this->_logger = $logger;
    }

    /**
     * Sales Order Creditmemo Save After event handler.
     *
     * @param \Magento\Framework\Event\Observer $observer
     */
    public function execute(\Magento\Framework\Event\Observer $observer)
    {
        $creditmemo = $observer->getCreditmemo();

        foreach ($creditmemo->getAllItems() as $item) {
            $onsale = $this->_onsaleFactory
                ->create()
                    ->getCollection()
                        ->addFieldToFilter('item_id', ['eq' => $item->getOrderItemId()])
                        ->addFieldToFilter('product_id', ['eq' => $item->getProductId()])
                        ->addFieldToFilter('order_id', ['eq' => $creditmemo->getOrderId()]);

            if (count($onsale)) {
                $onsale = $onsale->getFirstItem();
                $onsale
                    ->setQtyReturned($onsale->getQtyReturned() + $item->getQty())
                    ->save();
            }
        }
    }
}
