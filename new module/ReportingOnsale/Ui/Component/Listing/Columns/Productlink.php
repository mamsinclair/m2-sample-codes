<?php
/**
 * Product name links on Onsale Admin Grid
 *
 * @category    Magento2 module
 * @package     Shopcreator_ReportingOnsale
 * @version     1.0.0.0
 * @author      Shopcreator
 * @author      Robert Szeker
 * @copyright   Copyright (c) 1998-2017 Shopcreator (https://www.shopcreator.com/)
 * @copyright   Copyright (c) 2014-2017 Simple Evolve Ltd.
 * @license     https://www.shopcreator.com/license.html
 */
namespace Shopcreator\ReportingOnsale\Ui\Component\Listing\Columns;

class Productlink
    extends \Magento\Ui\Component\Listing\Columns\Column {
    /**
     * @var \Magento\Framework\UrlInterface
     */
    protected $_urlBuilder;

    /**
     * @var \Shopcreator\ReportingOnsale\Ui\Component\Listing\Columns\Options\ProductsByName
     */
    protected $_productsByName;

    /**
     * Constructor
     *
     * @param \Magento\Framework\View\Element\UiComponent\ContextInterface
     * @param \Magento\Framework\View\Element\UiComponentFactory
     * @param \Magento\Framework\UrlInterface
     * @param \Shopcreator\ReportingOnsale\Ui\Component\Listing\Columns\Options\ProductsByName
     * @param array $components
     * @param array $data
     */
    public function __construct(
        \Magento\Framework\View\Element\UiComponent\ContextInterface $context,
        \Magento\Framework\View\Element\UiComponentFactory $uiComponentFactory,
        \Magento\Framework\UrlInterface $urlBuilder,
        \Shopcreator\ReportingOnsale\Ui\Component\Listing\Columns\Options\ProductsByName $productsByName,
        array $components = [],
        array $data = []
    ) {
        $this->_urlBuilder = $urlBuilder;
        $this->_productsByName = $productsByName;
        parent::__construct($context, $uiComponentFactory, $components, $data);
    }

    /**
     * Prepare Data Source
     *
     * @param array $dataSource
     * @return array
     */
    public function prepareDataSource(array $dataSource)
    {
        if (isset($dataSource['data']['items'])) {
            $fieldName = $this->getData('name');
            foreach ($dataSource['data']['items'] as &$item) {
                if (isset($item['entity_id'])) {
                    $item[$fieldName] =
                        "<a href='".
                            $this->_urlBuilder->getUrl('catalog_product_edit',['id' => $item['product_id']]).
                            "' title='".__('View Product')."' target='_blank'>".
                            $this->_productsByName->getProductNameById($item['product_id']).
                        '</a>';
                }
            }
        }

        return $dataSource;
    }
}
