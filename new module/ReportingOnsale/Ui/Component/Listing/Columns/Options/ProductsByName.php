<?php
/**
 * Product names for admin grid filter
 *
 * @category    Magento2 module
 * @package     Shopcreator_ReportingOnsale
 * @version     1.0.0.0
 * @author      Shopcreator
 * @author      Robert Szeker
 * @copyright   Copyright (c) 1998-2017 Shopcreator (https://www.shopcreator.com/)
 * @copyright   Copyright (c) 2014-2017 Simple Evolve Ltd.
 * @license     https://www.shopcreator.com/license.html
 */
namespace Shopcreator\ReportingOnsale\Ui\Component\Listing\Columns\Options;

class ProductsByName
    implements \Magento\Framework\Data\OptionSourceInterface {
    /*
     * @var \Magento\Framework\Escaper
     */
    protected $_escaper;

    /**
     * @var \Magento\Catalog\Api\ProductRepositoryInterface
     */
    protected $_productRepository;

    /**
     * @var \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     */
    protected $_configurableChecker;

    /**
     * @var \Psr\Log\LoggerInterface
     */
    protected $_logger;

    /*
     * @var \Shopcreator\ReportingOnsale\Api\Data\OnsaleIndexInterfaceFactory
     */
    protected $_onsaleIndexFactory;

    /**
     * Constructor
     *
     * @param \Magento\Framework\Escaper
     * @param \Magento\Catalog\Api\ProductRepositoryInterface
     * @param \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable
     * @param \Shopcreator\ReportingOnsale\Api\Data\OnsaleIndexInterfaceFactory
     * @param \Psr\Log\LoggerInterface
     */
    public function __construct(
        \Magento\Framework\Escaper $escaper,
        \Magento\Catalog\Api\ProductRepositoryInterface $productRepository,
        \Magento\ConfigurableProduct\Model\ResourceModel\Product\Type\Configurable $configurableChecker,
        \Shopcreator\ReportingOnsale\Api\Data\OnsaleIndexInterfaceFactory $onsaleIndexFactory,
        \Psr\Log\LoggerInterface $logger
    )
    {
        $this->_escaper = $escaper;
        $this->_productRepository = $productRepository;
        $this->_configurableChecker = $configurableChecker;
        $this->_onsaleIndexFactory = $onsaleIndexFactory;
        $this->_logger = $logger;
    }

    /**
     * Get options
     *
     * @return array
     */
    public function toOptionArray()
    {
        $options = [];
        $collection = $this->_onsaleIndexFactory->create()->getCollection()
            ->setOrder('product_id', 'ASC');

        foreach ($collection as $onsale_product) {
            $productName = $this->getProductNameById($onsale_product->getProductId());

            $options[] = [
                'value'=> $onsale_product->getProductId(),//$productName,
                'label'=> $productName
            ];
        }
        return $options;
    }

    /**
     * Get product name or parent's name with size attribute
     *
     * @param  integer $product_id
     * @return string
     */
    public function getProductNameById($product_id) {
        $product = $this->_productRepository
            ->getById($product_id);

        $parent = $this->_configurableChecker
            ->getParentIdsByChild($product_id);

        if (isset($parent[0])){
            $attr = $product->getResource()->getAttribute('size');
            if ($attr->usesSource()) {
                $optionText = ' - Size: '.$attr->getSource()->getOptionText($product->getSize());
            } else {
                $optionText ='';
            }
            $productName = $this->_productRepository
                ->getById($parent[0])
                    ->getName().$optionText;
        } else {
            $productName = $product->getName();
        }

        return $productName;
        return $this->_escaper->escapeHtml($productName);
    }
}
