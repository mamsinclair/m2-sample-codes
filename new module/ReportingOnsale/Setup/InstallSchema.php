<?php
/**
 * Install databases
 *
 * @category    Magento2 module
 * @package     Shopcreator_ReportingOnsale
 * @version     1.0.0.0
 * @author      Shopcreator
 * @author      Robert Szeker
 * @copyright   Copyright (c) 1998-2017 Shopcreator (https://www.shopcreator.com/)
 * @copyright   Copyright (c) 2014-2017 Simple Evolve Ltd.
 * @license     https://www.shopcreator.com/license.html
 */
namespace Shopcreator\ReportingOnsale\Setup;

use Magento\Framework\Setup\InstallSchemaInterface;
use Magento\Framework\Setup\ModuleContextInterface;
use Magento\Framework\Setup\SchemaSetupInterface;

/**
 * @codeCoverageIgnore
 */
class InstallSchema implements InstallSchemaInterface
{
    /**
     * {@inheritdoc}
     * @SuppressWarnings(PHPMD.ExcessiveMethodLength)
     */
    public function install(SchemaSetupInterface $setup, ModuleContextInterface $context)
    {
        echo "\n\n\n--- Install Schema ---\n";
        $setup->startSetup();

        $this
            ->createOnsaleReportTable($setup)
            ->createOnsaleIndexTable($setup);

        $setup->endSetup();
        echo "--- Install Schema done ---\n\n";
    }

    /**
     * Create table '_shopcreator_reporting_onsale'
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $installer
     * @return $this
     */
    private function createOnsaleReportTable(\Magento\Framework\Setup\SchemaSetupInterface $installer) {
        /**
         *
         */
        $table = $installer->getConnection()->newTable(
            $installer->getTable('_shopcreator_reporting_onsale')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            'product_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Product'
        )->addColumn(
            'store_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => 0],
            'Store'
        )->addColumn(
            'order_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => 0],
            'Order'
         )->addColumn(
            'item_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => 0],
            'Order item'
        )->addColumn(
            'qty',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['unsigned' => true, 'nullable' => false, 'default' => 0],
            'Quantity'
        )->addColumn(
            'qty_returned',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['unsigned' => true, 'nullable' => false, 'default' => 0],
            'Quantity'
        )->addColumn(
            'price',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['unsigned' => true, 'nullable' => false, 'default' => 0],
            'Original Price'
        )->addColumn(
            'special_price',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['unsigned' => true, 'nullable' => false, 'default' => 0],
            'Special Price'
        )->addColumn(
            'sale_value',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['unsigned' => true, 'nullable' => false, 'default' => 0],
            'Sale value'
        )->setComment(
            "Onsale report raw data"
        )->addIndex(
            $installer->getIdxName(
                '_shopcreator_reporting_onsale',
                ['product_id'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
            ),
            ['product_id'],
            ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX]
        );
        $installer->getConnection()->createTable($table);
        echo "    Table `_shopcreator_reporting_onsale` created\n";

        return $this;
    }

    /**
     * Create table '_shopcreator_reporting_onsale_index'
     *
     * @param \Magento\Framework\Setup\SchemaSetupInterface $installer
     * @return $this
     */
    private function createOnsaleIndexTable(SchemaSetupInterface $installer) {
        $table = $installer->getConnection()->newTable(
            $installer->getTable('_shopcreator_reporting_onsale_index')
        )->addColumn(
            'entity_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['identity' => true, 'unsigned' => true, 'nullable' => false, 'primary' => true],
            'ID'
        )->addColumn(
            'product_id',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false],
            'Product'
        )->addColumn(
            'number_of_orders',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => 0],
            'Number of orders'
        )->addColumn(
            'number_of_products',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => 0],
            'Number of product sold'
        )->addColumn(
            'number_of_products_returned',
            \Magento\Framework\DB\Ddl\Table::TYPE_INTEGER,
            null,
            ['unsigned' => true, 'nullable' => false, 'default' => 0],
            'Number of product returned'
        )->addColumn(
            'sales_value',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['unsigned' => true, 'nullable' => false, 'default' => 0],
            'Total sales value'
        ) ->addColumn(
            'returned_value',
            \Magento\Framework\DB\Ddl\Table::TYPE_DECIMAL,
            '12,4',
            ['unsigned' => true, 'nullable' => false, 'default' => 0],
            'Total value of returned products'
        )->setComment(
            "Onsale report index data"
        )->addIndex(
            $installer->getIdxName(
                '_shopcreator_reporting_onsale_index',
                ['product_id'],
                \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_INDEX
            ),
            ['product_id'],
            ['type' => \Magento\Framework\DB\Adapter\AdapterInterface::INDEX_TYPE_UNIQUE]
        );
        $installer->getConnection()->createTable($table);
        echo "    Table `_shopcreator_reporting_onsale_index` created\n";

        return $this;
    }

}
